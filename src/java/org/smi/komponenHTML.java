/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.smi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author iweks24
 */
public class komponenHTML {

    public komponenHTML() {

    }

    public String option(Connection kon, String sql) {
        String pilih = "";
        try {
            PreparedStatement stat = kon.prepareStatement(sql);
            ResultSet rset = stat.executeQuery();

            while (rset.next()) {
                pilih = pilih + "<option value=\"" + rset.getString(1) + "\">&nbsp;&nbsp;" + rset.getString(2) + "&nbsp;&nbsp;</option>";
            }
        } catch (SQLException ex) {
            Logger.getLogger(komponenHTML.class.getName()).log(Level.SEVERE, null, ex);
        }

        return pilih;
    }
}
