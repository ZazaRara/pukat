/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.smi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.CellStyle;

/**
 *
 * @author iweks
 */
public class koneksiDatabase {

    public koneksiDatabase() {

    }

    public Connection getKoneksiSumber() {
        Connection kon = null;
        boolean driver = false;
        String url = "jdbc:mysql://117.102.65.114:3306/pukat";
        String user = "javaglobal";
        String password = "javaglobalmandiri";
//        url = "jdbc:mysql://52.230.28.3:3306/pukat";
//        user = "root";
//        password = "0ajiMXCPlMI1";
//
//        url = "jdbc:mysql://52.187.56.198:3306/pukat";
//        user = "root";
//        password = "HLr1P0owYP9M";
        try {
            // Penyambungan Driver
            Class.forName("com.mysql.jdbc.Driver");
            driver = true;
            // Apabila Kelas Driver Tidak Ditemukan
        } catch (ClassNotFoundException not) {
            //  koneksi = null;
            System.out.println(not);
        }

        if (driver == true) {

            try {
                kon = java.sql.DriverManager.getConnection(url, user, password);

            } // Akhir try
            catch (java.sql.SQLException sqln) {
                //    koneksi = null;
                System.out.println(sqln);

            }
        }
        return kon;
    }

    public Connection getKoneksiTujuan() {
        Connection kon = null;
        boolean driver = false;
        String url = "jdbc:mysql://117.102.65.114:3306/pukat";
        String user = "javaglobal";
        String password = "javaglobalmandiri";
//        url = "jdbc:mysql://52.230.28.3:3306/pukat";
//        user = "root";
//        password = "0ajiMXCPlMI1";
//
//        url = "jdbc:mysql://52.187.56.198:3306/pukat";
//        user = "root";
//        password = "HLr1P0owYP9M";
        try {
            // Penyambungan Driver
            Class.forName("com.mysql.jdbc.Driver");
            driver = true;
            // Apabila Kelas Driver Tidak Ditemukan
        } catch (ClassNotFoundException not) {
            //  koneksi = null;
            System.out.println(not);
        }

        if (driver == true) {

            try {
                kon = java.sql.DriverManager.getConnection(url, user, password);

            } // Akhir try
            catch (java.sql.SQLException sqln) {
                //    koneksi = null;
                System.out.println(sqln);

            }
        }
        return kon;
    }

    public Connection getKoneksiSystem() {
        Connection kon = null;
        boolean driver = false;
        String url = "jdbc:mysql://117.102.65.114:3306/pukat";
        String user = "javaglobal";
        String password = "javaglobalmandiri";
//        url = "jdbc:mysql://52.230.28.3:3306/pukat";
//        user = "root";
//        password = "0ajiMXCPlMI1";
//
//        url = "jdbc:mysql://52.187.56.198:3306/pukat";
//        user = "root";
//        password = "HLr1P0owYP9M";
        try {

            Class.forName("com.mysql.jdbc.Driver");
            driver = true;

        } catch (ClassNotFoundException not) {
            //  koneksi = null;
            System.out.println(not);
        }

        if (driver == true) {

            try {
                kon = java.sql.DriverManager.getConnection(url, user, password);

            } // Akhir try
            catch (java.sql.SQLException sqln) {
                //    koneksi = null;
                System.out.println(sqln);

            }
        }
        return kon;
    }

    public boolean updateData(Connection kon, String tabel, String[][] data, String filter) {
        boolean sukses = false;
        try {
            int y = data.length;
            String sKolom = "";
            //  String sIsi = "";
            for (int x = 0; x < y; x++) {
                String kolom = data[x][0].toString();
                String isi = data[x][1].toString();
                isi = isi.replaceAll("'", "\'");
                sKolom = sKolom + kolom + " = '" + isi + "',";
                //   sIsi = sIsi + "'" + isi + "',";
            }

            String sql = "UPDATE " + tabel
                    + " SET " + sKolom.substring(0, sKolom.length() - 1)
                    + " WHERE " + filter;

            PreparedStatement stat = kon.prepareStatement(sql);
            stat.executeUpdate();
            sukses = true;
        } catch (SQLException ex) {
            System.out.println(filter);
        }
        return sukses;
    }

    public boolean simpanDataBlob(Connection kon, String tabel, byte[] b) {
        boolean ok = false;
        try {
            PreparedStatement stat = kon.prepareStatement(tabel);
            stat.setBytes(1, b);
            stat.executeUpdate();
            ok = true;
        } catch (SQLException ex) {
            Logger.getLogger(koneksiDatabase.class.getName()).log(Level.SEVERE, null, ex);
        }

        return ok;
    }

    public boolean simpanData(Connection kon, String tabel, String[][] data, boolean from) {
        boolean sukses = false;
        try {
            int y = data.length;
            String sKolom = "";
            String sIsi = "";
            for (int x = 0; x < y; x++) {
                String kolom = data[x][0].toString();
                String isi = data[x][1].toString();
                isi = isi.replaceAll("'", "\'");
                sKolom = sKolom + kolom + ",";
                sIsi = sIsi + "'" + isi + "',";
            }
            if (from) {
                sKolom = sKolom + "from_date ";
                sIsi = sIsi + "now() ";
            }
            String sql = "INSERT INTO " + tabel
                    + " (" + sKolom.substring(0, sKolom.length() - 1)
                    + ") VALUES (" + sIsi.substring(0, sIsi.length() - 1) + ")";

            PreparedStatement stat = kon.prepareStatement(sql);
            stat.executeUpdate();
            sukses = true;
        } catch (SQLException ex) {
            //   Logger.getLogger(penangananKomponen.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sukses;
    }

    public int simpanDataReturn(Connection kon, String tabel, String[][] data, boolean from) {
        int sukses = 0;
        try {
            int y = data.length;
            String sKolom = "";
            String sIsi = "";
            for (int x = 0; x < y; x++) {
                String kolom = data[x][0].toString();
                String isi = data[x][1].toString();
                isi = isi.replaceAll("'", "\'");
                sKolom = sKolom + kolom + ",";
                sIsi = sIsi + "'" + isi + "',";
            }
            if (from) {
                sKolom = sKolom + "from_date ";
                sIsi = sIsi + "now() ";
            }
            String sql = "INSERT INTO " + tabel
                    + " (" + sKolom.substring(0, sKolom.length() - 1)
                    + ") VALUES (" + sIsi.substring(0, sIsi.length() - 1) + ")";

            PreparedStatement stat = kon.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            stat.executeUpdate();
            ResultSet rs = stat.getGeneratedKeys();
            if (rs.next()) {
                sukses = rs.getInt(1);
            }

        } catch (SQLException ex) {
            //   Logger.getLogger(penangananKomponen.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sukses;
    }

    public int simpanDataReturnUUID(Connection kon, String tabel, String[][] data, boolean from) {
        int sukses = 0;
        try {
            int y = data.length;
            String sKolom = "";
            String sIsi = "";
            for (int x = 0; x < y; x++) {
                String kolom = data[x][0].toString();
                String isi = data[x][1].toString();
                isi = isi.replaceAll("'", "\'");
                sKolom = sKolom + kolom + ",";
                sIsi = sIsi + "'" + isi + "',";
            }
            if (from) {
                sKolom = sKolom + "from_date ";
                sIsi = sIsi + "now() ";
            }
            String sql = "INSERT INTO " + tabel
                    + " (" + sKolom.substring(0, sKolom.length() - 1)
                    + ", guid) VALUES (" + sIsi.substring(0, sIsi.length() - 1) + ", UUID())";

            PreparedStatement stat = kon.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
            stat.executeUpdate();
            ResultSet rs = stat.getGeneratedKeys();
            if (rs.next()) {
                sukses = rs.getInt(1);
            }

        } catch (SQLException ex) {
            //   Logger.getLogger(penangananKomponen.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sukses;
    }

    public Object[] getDataFromSQL(Connection kon, String query) {
        Object[] data = null;

        try {

            Statement stat = kon.createStatement();
            ResultSet rs = stat.executeQuery(query);

            int columns = rs.getMetaData().getColumnCount();
            if (rs.next()) {
                data = new Object[columns + 1];

                for (int i = 1; i <= columns; i++) {
                    data[i - 1] = rs.getObject(i);
                }

            }
        } catch (SQLException ex) {
//            Logger.getLogger(penangananKomponen.class.getName()).log(Level.SEVERE, null, ex);
        }
        return data;
    }

    public String getDataJSONFromSQL(Connection kon, String query) {
        String data = "";
        String koma = ", ";

        try {
            PreparedStatement stat = kon.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = stat.executeQuery();

            int columns = rs.getMetaData().getColumnCount();
            int nomor = 0;
            while (rs.next()) {
                nomor++;
                String bhku = "{\"no\" : \"" + nomor + "\", ";
                String bhku_koma = ", ";
                if (rs.isLast()) {
                    koma = "";
                }
                for (int i = 1; i <= columns; i++) {
                    Object xx = rs.getObject(i);
                    String a = "-";
                    if (xx != null) {
                        a = rs.getObject(i).toString();
                    }
                    String nama = rs.getMetaData().getColumnName(i);

                    if (i == columns) {
                        bhku_koma = "}";
                    }
                    bhku = bhku + "\"" + nama + "\" : \"" + a + "\"" + bhku_koma;
                }
                data = data + bhku + koma;
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return data;
    }

    public String getHeaderTabel(String[] data) {
        String hasil = "";
        int y = data.length;
        hasil = hasil + "<th style=\"width: 10px;\">No</th>";
        for (int x = 0; x < y; x++) {
            String kolom = data[x].toString();
            hasil = hasil + "<th>" + kolom + "</th>";

        }
        return hasil;
    }

    public String getDataHTMLTabel(Connection kon, String query) {
        String data = "";
        String koma = "</tr>";

        try {

            PreparedStatement stat = kon.prepareStatement(query, ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
            ResultSet rs = stat.executeQuery();

            int columns = rs.getMetaData().getColumnCount();
            int nomor = 0;
            while (rs.next()) {
                nomor++;
                String bhku = "<tr><td>" + nomor + "</td>";

                for (int i = 1; i <= columns; i++) {
                    Object xx = rs.getObject(i);
                    String a = "";
                    if (xx != null) {
                        a = rs.getObject(i).toString();
                    }

                    if (a.equalsIgnoreCase("cekbox")) {
                        a = "<input type=\"checkbox\" name=\"cek_" + nomor + "\" value=\"true\">";
                    }
                    bhku = bhku + "<td>" + a + "</td>";
                }
                data = data + bhku + koma;
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return data;
    }

    public boolean setSQL(Connection kon, String sql) {
        boolean sukses = false;
        try {
            PreparedStatement stat = kon.prepareStatement(sql);
            stat.executeUpdate();
            sukses = true;
        } catch (SQLException ex) {
            //     Logger.getLogger(penangananKomponen.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sukses;

    }

    public void createHeaderTabelExcel(String[][] data, HSSFSheet sheet, HSSFRow row, HSSFCell cell, CellStyle cs3, int bb) {
        int y = data.length;
        row = sheet.createRow(bb);
        for (int x = 0; x < y; x++) {
            String kolom = data[x][0].toString();
            String isi = data[x][1].toString();
            sheet.setColumnWidth((short) (x), (short) (short) ((50 * Integer.parseInt(isi)) / ((double) 1 / 20)));
            cell = row.createCell(x);
            cell.setCellStyle(cs3);
            cell.setCellValue(kolom);
        }
    }

    public void setTabelExcel(Connection kon, String Query, HSSFSheet sheet, HSSFRow rowx, HSSFCell cell, int bb) {
        try {

            Statement stat = kon.createStatement();
            ResultSet rs = stat.executeQuery(Query);

            int columns = rs.getMetaData().getColumnCount();
            //  System.out.println(columns);
            int yh = 1;
            while (rs.next()) {
                rowx = sheet.createRow(bb);
                cell = rowx.createCell(0);
                cell.setCellValue(yh);
                for (int i = 1; i <= columns; i++) {
                    String data = rs.getObject(i).toString();
                    cell = rowx.createCell(i);
                    cell.setCellValue(data);
                }
                bb++;
                yh++;
            }

            rs.close();
            stat.close();
            // conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public synchronized String getStringSQL(Connection kon, String sql) {
        String hasil = "0";
        //  System.out.println(sql);
        try {
            //  Connection kon = getKoneksi();

            PreparedStatement stat = kon.prepareStatement(sql);
            ResultSet rset = stat.executeQuery();
            int x = 0;
            if (rset.next()) {
                hasil = rset.getString(1);
                if (hasil == null) {
                    hasil = "";
                }
            }
            rset.close();
            stat.close();
            // kon.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return hasil;
    }

    public synchronized int getIntSQL(Connection kon, String sql) {
        int hasil = 0;
        try {
            //  Connection kon = getKoneksi();

            PreparedStatement stat = kon.prepareStatement(sql);
            ResultSet rset = stat.executeQuery();
            int x = 0;
            if (rset.next()) {
                hasil = rset.getInt(1);

            }
            rset.close();
            stat.close();
            // kon.close();
        } catch (SQLException ex) {
            //  Logger.getLogger(utamaSurat.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hasil;
    }

    public String getStringFilter(String[] aku, String f) {
        String a = "";
        int y = aku.length;
        for (int x = 0; x < y; x++) {
            String kolom = aku[x].toString();
            a = a + "OR " + kolom + " LIKE '%" + f + "%' ";
        }
        a = a.replaceFirst("OR", "WHERE");
        return a;
    }

}
