/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.smi;

import java.sql.Connection;
import java.util.Map;

/**
 *
 * @author iweks
 */
public class simpanData {

    koneksiDatabase kb = new koneksiDatabase();

    public simpanData() {

    }

    public boolean simpanTransaksi(String userid, String jenis, String keterangan, String nominal, String bukti, String orang) {
        boolean ok = false;
        Connection kon = kb.getKoneksiTujuan();
        int saldo = kb.getIntSQL(kon, "SELECT TOP 1 saldo FROM transaksi WHERE userid = '" + userid + "' ORDER BY id DESC");
        if (jenis.equalsIgnoreCase("masuk")) {
            saldo = saldo + Integer.parseInt(nominal);
            String ins = "INSERT INTO transaksi (userid, tanggal, keterangan, masuk, keluar, saldo, create_user,bukti) "
                    + "VALUES('" + userid + "',getdate(),'" + keterangan + "','" + nominal + "','0','" + saldo + "','" + orang + "','" + bukti + "')";
            ok = kb.setSQL(kon, ins);
        } else if (jenis.equalsIgnoreCase("keluar")) {
            saldo = saldo - Integer.parseInt(nominal);

            String ins = "INSERT INTO transaksi (userid, tanggal, keterangan, masuk, keluar, saldo, create_user, bukti) "
                    + "VALUES('" + userid + "',getdate(),'" + keterangan + "','0','" + nominal + "','" + saldo + "','" + orang + "','" + bukti + "')";
            ok = kb.setSQL(kon, ins);
        }
        return ok;
    }

    public boolean simpanJasa(String userid, String keterangan, String nominal, String orang) {
        boolean ok = false;
        Connection kon = kb.getKoneksiTujuan();
        String upd = "UPDATE biaya SET thru_date = getdate() WHERE kode = '" + userid + "' AND thru_date IS NULL";
        String ins = "INSERT INTO biaya (kode, biaya, keterangan, create_user) "
                + "VALUES('" + userid + "','" + nominal + "','" + keterangan + "','" + orang + "')";
        kb.setSQL(kon, upd);
        ok = kb.setSQL(kon, ins);

        return ok;
    }

    public boolean simpanApproval(Map<String, String[]> parameters, String hak) {
        boolean ok = true;
        Connection kon = kb.getKoneksiTujuan();
        Connection konIns = kb.getKoneksiSumber();
        String[] a = parameters.get("cek");
        String[] b = parameters.get("user");
        String[] c = parameters.get("kode");
        String[] d = parameters.get("tanggal");
        String[] e = parameters.get("jumlah");
        String[] f = parameters.get("samsat");
        String[] g = parameters.get("bukti");

        int size = a.length;

        for (int i = 0; i < size; i++) {
            int y = Integer.parseInt(a[i].toString());
            String user = b[y].toString();
            String kode = c[y].toString();
            String tanggal = d[y].toString();
            String jumlah = e[y].toString();
            String samsat = f[y].toString();
            String bukti = g[y].toString();

            int biaya = kb.getIntSQL(kon, "SELECT TOP 1 biaya FROM v_biaya_aktif WHERE kode = '" + kode + "'");
            int saldo = kb.getIntSQL(kon, "SELECT TOP 1 saldo FROM transaksi WHERE userid = '" + user + "' ORDER BY id DESC");

            String keluar = String.valueOf(Integer.parseInt(jumlah) * biaya);
            String saldo_akhir = String.valueOf(saldo - Integer.parseInt(keluar));
            String ins = "INSERT INTO transaksi(userid, tanggal, bukti, masuk, keluar, saldo, kode, unit, satuan, keterangan, create_user) "
                    + "VALUES('" + user + "','" + tanggal + "','" + bukti + "','0','" + keluar + "','" + saldo_akhir + "','" + kode
                    + "','" + jumlah + "','" + biaya + "','" + samsat + " : " + jumlah + " UNIT" + "','" + hak + "')";
            boolean cek = kb.setSQL(kon, ins);
            if (!cek) {
                ok = false;
            } else {
                String upd = "INSERT INTO bbn.support_cek(userid, kode, tanggal, create_by, no_surat_tugas) "
                        + "VALUES('" + user + "','" + kode + "','" + tanggal + "','" + hak + "','" + bukti + "')";
                kb.setSQL(konIns, upd);
            }
        }
        return ok;
    }

}
