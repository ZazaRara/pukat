/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.smi;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Iweks
 */
public class LoginSystem {

    public static void main(String arg[]) {
        try {
            LoginSystem apa = new LoginSystem();
            String a = apa.getCryptPassword("HALLO");
            System.out.println(a);
        } catch (Exception ex) {
            Logger.getLogger(LoginSystem.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public LoginSystem() {
    }

    public String getLogin(String user, String sandi) {
        String hasil = "login.jsp";
        try {

            String pass = sandi;
            koneksiDatabase kb = new koneksiDatabase();
            Connection kon = kb.getKoneksiSystem();
            int ok = kb.getIntSQL(kon, "SELECT count(1) as jml FROM musik_user WHERE userid = '" + user
                    + "' AND pass = MD5('" + pass + "')");
            if (ok > 0) {
                hasil = "sukses.jsp";
            }
        } catch (Exception ex) {
            Logger.getLogger(LoginSystem.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hasil;
    }

    public String getCryptPassword(String plainPass)
            throws Exception {
        String sRet = null;
        if (plainPass != null && plainPass.length() != 0) {
            try {
                MessageDigest md5 = MessageDigest.getInstance("MD5");
                byte[] md5Hash = md5.digest(plainPass.getBytes());
                sRet = convToString(md5Hash, md5Hash.length * 8);
            } catch (Exception e) {
                throw new Exception(e.getLocalizedMessage());
            }
        }
        return sRet;
    }

    private String convToString(byte[] b, int numBits) {
        StringBuffer stringBuffer = new StringBuffer();
        int numBytes = (int) Math.floor((double) numBits / 8D);
        int numChars = (int) Math.floor((double) numBits / 5D);
        int[] dst = new int[numBytes];
        String retVal;
        try {
            DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream(b));
            for (int i = numBytes - 1; i >= 0; i--) {
                dst[i] = dataInputStream.readUnsignedByte();
            }

            for (int i = numChars - 1; i >= 0; i--) {
                int bitNumber = i * 5;
                int byteNumber = bitNumber / 8;
                int pos = 8 - bitNumber % 8;
                int index;
                if (pos == 5) {
                    index = dst[byteNumber] & 0x1f;
                } else if (pos > 5) {
                    index = dst[byteNumber] >> pos - 5 & 0x1f;
                } else {
                    index = dst[byteNumber] << 5 - pos & 0x1f | dst[byteNumber + 1] >> 3 + pos & 0x1f;
                }
                stringBuffer.append(CONVERSION_TABLE[index]);
            }

            retVal = stringBuffer.toString();
        } catch (IOException ioe) {
            retVal = null;
        }
        return retVal;
    }
    private static final char[] CONVERSION_TABLE = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
        'A', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L',
        'M', 'N', 'P', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
        'Y', 'Z'
    };
}
