/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.smi;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author iweks
 */
public class getDataGrafik {

    String kat = "";
    String hasil = "";

    public getDataGrafik() {

    }

    public void setQueryDataGrafik(Connection kon, String sql) {
        kat = "";
        hasil = "";
        try {
            PreparedStatement stat = kon.prepareStatement(sql);
            ResultSet rset = stat.executeQuery();
            while (rset.next()) {
                String a = rset.getString(1);
                String b = rset.getString(2);
                hasil = hasil + b + ",";
                kat = kat + "\'" + a + "\',";
            }
            hasil = "[" + hasil.substring(0, hasil.length() - 1) + "]";
            kat = "[" + kat.substring(0, kat.length() - 1) + "]";
        } catch (SQLException ex) {
            Logger.getLogger(getDataGrafik.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public String getKategori() {
        return kat;
    }

    public String getData() {
        return hasil;
    }

    public String getDataTabelGrafik(Connection kon, String sql) {
        String hasil = "";
        try {
            PreparedStatement stat = kon.prepareStatement(sql);
            ResultSet rset = stat.executeQuery();
            while (rset.next()) {
                String a = rset.getString(1);
                String b = rset.getString(2);

                hasil = hasil + "<tr role=\"row\" class=\"abu-muda\"><td>" + a + "</td><td style=\"width: 35px;text-align: right\">" + b + "</td></tr>";
                //     kat = kat + "\'" + a + "\',";
            }
            //   hasil = "[" + hasil.substring(0, hasil.length() - 1) + "]";
            //   kat = "[" + kat.substring(0, kat.length() - 1) + "]";
        } catch (SQLException ex) {
            Logger.getLogger(getDataGrafik.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hasil;
    }

    public String getDataTabelTagihan(Connection kon) {
        String sql = "SELECT date_format(a.periode,'%Y-%m'), sum(a.jumlah) as tagihan, sum(b.jumlah - b.diskon) as bayar, sum(b.diskon) as diskon, sum(a.jumlah) - sum(IFNULL(b.jumlah, 0)) as belum FROM musik_tagihan_siswa a LEFT JOIN musik_pembayaran_tagihan_siswa b ON b.id_tagihan = a.id_tagihan WHERE a.periode >= '2016-01-01' AND a.periode <= now() GROUP BY date_format(a.periode,'%Y-%m') ORDER BY date_format(a.periode,'%Y-%m')";
        komponen kom = new komponen();
        String hasil = "";
        try {
            PreparedStatement stat = kon.prepareStatement(sql);
            ResultSet rset = stat.executeQuery();
            int x = 0;
            while (rset.next()) {
                x++;
                String a = rset.getString(1);
                String b = rset.getString(2);
                String c = rset.getString(3);
                String d = rset.getString(4);
                String e = rset.getString(5);

                b = kom.ribuan(b);
                c = kom.ribuan(c);
                d = kom.ribuan(d);
                e = kom.ribuan(e);
                hasil = hasil + "<tr role=\"row\" class=\"abu-muda\"><td>" + x + "</td><td style=\"text-align: left\">" + a + "</td><td style=\"text-align: center\">" + b + "</td><td style=\"text-align: center\">" + c + "</td><td style=\"text-align: center\">" + d + "</td><td style=\"text-align: center\">" + e + "</td></tr>";

                //     kat = kat + "\'" + a + "\',";
            }

            //   hasil = "[" + hasil.substring(0, hasil.length() - 1) + "]";
            //   kat = "[" + kat.substring(0, kat.length() - 1) + "]";
        } catch (SQLException ex) {
            Logger.getLogger(getDataGrafik.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hasil;
    }
    public String getDataTabelPembayaran(Connection kon) {
        String sql = "SELECT date_format(a.tanggal_bayar,'%Y-%m'), sum(a.jumlah - a.diskon) as bayar, sum(CASE WHEN b.id IS NULL THEN 0 ELSE a.jumlah - a.diskon END) as diskon,sum(a.jumlah - a.diskon) - sum(CASE WHEN b.id IS NULL THEN 0 ELSE a.jumlah - a.diskon END) as belum FROM musik_pembayaran_tagihan_siswa a LEFT JOIN musik_pembayaran_tagihan_siswa_approve b ON b.id = a.id WHERE a.tanggal_bayar >= '2016-01-01' AND a.tanggal_bayar <= now() AND a.tipe_pembayaran = 'SPP' GROUP BY date_format(a.tanggal_bayar,'%Y-%m') ORDER BY date_format(a.tanggal_bayar,'%Y-%m')";
        komponen kom = new komponen();
        String hasil = "";
        try {
            PreparedStatement stat = kon.prepareStatement(sql);
            ResultSet rset = stat.executeQuery();
            int x = 0;
            while (rset.next()) {
                x++;
                String a = rset.getString(1);
                String b = rset.getString(2);
                String c = rset.getString(3);
                String d = rset.getString(4);
            //    String e = rset.getString(5);

                b = kom.ribuan(b);
                c = kom.ribuan(c);
                d = kom.ribuan(d);
             //   e = kom.ribuan(e);
                hasil = hasil + "<tr role=\"row\" class=\"abu-muda\"><td>" + x + "</td><td style=\"text-align: left\">" + a + "</td><td style=\"text-align: center\">" + b + "</td><td style=\"text-align: center\">" + c + "</td><td style=\"text-align: center\">" + d + "</td></tr>";

                //     kat = kat + "\'" + a + "\',";
            }

            //   hasil = "[" + hasil.substring(0, hasil.length() - 1) + "]";
            //   kat = "[" + kat.substring(0, kat.length() - 1) + "]";
        } catch (SQLException ex) {
            Logger.getLogger(getDataGrafik.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hasil;
    }
    public String getDataTabelPembayaranPendaftaran(Connection kon) {
        String sql = "SELECT date_format(a.tanggal_bayar,'%Y-%m'), sum(a.jumlah - a.diskon) as bayar, sum(CASE WHEN b.id IS NULL THEN 0 ELSE a.jumlah - a.diskon END) as diskon,sum(a.jumlah - a.diskon) - sum(CASE WHEN b.id IS NULL THEN 0 ELSE a.jumlah - a.diskon END) as belum FROM musik_pembayaran_tagihan_siswa a LEFT JOIN musik_pembayaran_tagihan_siswa_approve b ON b.id = a.id WHERE a.tanggal_bayar >= '2016-01-01' AND a.tanggal_bayar <= now() AND a.tipe_pembayaran = 'PENDAFTARAN' GROUP BY date_format(a.tanggal_bayar,'%Y-%m') ORDER BY date_format(a.tanggal_bayar,'%Y-%m')";
        komponen kom = new komponen();
        String hasil = "";
        try {
            PreparedStatement stat = kon.prepareStatement(sql);
            ResultSet rset = stat.executeQuery();
            int x = 0;
            while (rset.next()) {
                x++;
                String a = rset.getString(1);
                String b = rset.getString(2);
                String c = rset.getString(3);
                String d = rset.getString(4);
            //    String e = rset.getString(5);

                b = kom.ribuan(b);
                c = kom.ribuan(c);
                d = kom.ribuan(d);
             //   e = kom.ribuan(e);
                hasil = hasil + "<tr role=\"row\" class=\"abu-muda\"><td>" + x + "</td><td style=\"text-align: left\">" + a + "</td><td style=\"text-align: center\">" + b + "</td><td style=\"text-align: center\">" + c + "</td><td style=\"text-align: center\">" + d + "</td></tr>";

                //     kat = kat + "\'" + a + "\',";
            }

            //   hasil = "[" + hasil.substring(0, hasil.length() - 1) + "]";
            //   kat = "[" + kat.substring(0, kat.length() - 1) + "]";
        } catch (SQLException ex) {
            Logger.getLogger(getDataGrafik.class.getName()).log(Level.SEVERE, null, ex);
        }
        return hasil;
    }
}
