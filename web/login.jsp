<%

    Object error = session.getAttribute("pesanError");
    String err = "";
    if (error != null) {
        err = "<br /><h5 style='color:blue;' align='center'>" + error
                + "</h5><input type='hidden' name='errorku' id='errorku' value='GAGAL'>";
    }
%>
<div class="container">
    <div class="row">
        <div class="panel-heading" style="background-color: #EEEEEE; text-align: center;">                                               
            <img src="img/logopukat.jpeg" height="130px" class="hidden-xs" />
            <h2 class="visible-xs" style="color: #303C5C; text-align: center;">PUKAT SEMARANG</h2>
        </div>
        <div class="col-md-6 col-md-offset-3 col-xs-12">
            <div class="panel panel-default" style="border:1px solid #EEEEEE;">

                <div class="panel-body" style="background-color: #303C5C;">
                    <%=err%>
                    <form action="#" autocomplete="on" method="post" accept-charset="UTF-8" role="form" class="form-signin">
                        <fieldset>
                            <label class="panel-login">
                                <div class="login_result"></div>
                            </label>
                            <input class="form-control" placeholder="Username" id="username" name="username" type="text">
                            <input class="form-control" placeholder="Password" id="sandi" name="sandi" type="password">
                            <br></br>
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="Login �">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
