<%-- 
    Document   : daftar
    Created on : Apr 2, 2017, 9:20:20 PM
    Author     : iweks24
--%>




<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.smi.koneksiDatabase"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%

    String userid = request.getParameter("q");

    koneksiDatabase kb = new koneksiDatabase();
    Connection kon = kb.getKoneksiSumber();
    String hasil = "";
    String nis = "";
    String nama = "";
    String panggil = "";
    String user = session.getAttribute("punyaku").toString();
    int hak = kb.getIntSQL(kon, "SELECT count(1) as jml FROM pukat_user_peran WHERE userid='" + user + "' AND peran='OPERATOR'");
    String id = "";
    if (hak <= 0) {
        response.sendRedirect("no.jsp");
    } else {
        try {
            id = kb.getStringSQL(kon, "SELECT id FROM pukat_pihak WHERE guid = '" + userid.toString() + "'");
            nis = kb.getStringSQL(kon, "SELECT member FROM pukat_pihak WHERE id = " + id);
            String sql = "SELECT atribut, nilai FROM pukat_pihak_detail WHERE id_daftar = '" + id + "'";
            PreparedStatement stat = kon.prepareStatement(sql);
            ResultSet rset = stat.executeQuery();
            while (rset.next()) {
                String a = rset.getString(1);
                String b = rset.getString(2);
                //     hasil = hasil + "$('#" + a + "').val('" + b + "'); \n";
                if (b.equalsIgnoreCase("ON")) {
                    hasil = hasil + "$('#" + a + "').prop('checked', true); \n";
                } else {
                    hasil = hasil + "$('#" + a + "').val('" + b + "'); \n";
                    if (a.equalsIgnoreCase("nama")) {
                        nama = b;
                    } else if (a.equalsIgnoreCase("panggilan")) {
                        panggil = b;
                    }
                }

            }
        } catch (SQLException e) {
            out.println(e.getMessage());
        }
    }
    kon.close();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PENDAFTARAN ANGGOTA</title>
        <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css'>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/skp_home.css" rel="stylesheet" type="text/css"/>
        <link href="css/daftar.css" rel="stylesheet" type="text/css"/>
        <link href="css/checkbox.css" rel="stylesheet" type="text/css"/>

        <script src="js/jquery.min.js"></script>
        <script src="js/nprogress.js"></script>
        <link href="css/login.css" rel="stylesheet" type="text/css"/>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>

    </head>
    <body style="background-color: white;">
        <form id="msform" action="setujuAnggota.jsp" method="POST" name="id" novalidate>
            <div class="col-lg-12" style="text-align:left; margin-top: -10px;">

                <div class="panel-3">

                    <img src="foto.jsp?kode=<%=userid%>" alt="" height="130px;" onerror="this.src='img/foto.gif'"/>
                </div>
                <div class="panel-7" style="font-size: 16;">
                    <input type="hidden" name="idku" value="<%=id%>" />
                    <input type="hidden" name="namaku" value="<%=nama%>" />
                    <div class="panel-4">NO. Anggota</div>
                    <div class="panel-8">: <strong><%=nis%></strong></div>
                    <div class="panel-4" style="margin-top: 10px;">N A M A</div>
                    <div class="panel-8" style="margin-top: 10px;">: <strong><%=nama%></strong></div>
                    <div class="panel-4" style="margin-top: 10px;">PANGGILAN</div>
                    <div class="panel-8" style="margin-top: 10px;">: <strong><%=panggil%></strong></div>
                    <div class="panel-4" style="margin-top: 10px;">Virtual Account BCA</div>
                    <div class="panel-8" style="margin-top: 10px;">: <strong><%=nis%></strong></div>

                </div>
                <div class="panel-2" style="padding-top: 40px;">

                    <input type="submit" name="submit" class="btn" value="SETUJU" style="background-color: green;" />   
                </div>
            </div>
            <div class="panel-12" style="margin-top: 5px;">
                <ul class="nav nav-tabs">
                    <li class="active"><a data-toggle="tab" href="#diri"><b>Pribadi</b></a></li>
                    <li><a data-toggle="tab" href="#a1"><b>Kontak</b></a></li>
                    <li><a data-toggle="tab" href="#a2"><b>Alamat</b></a></li>
                    <li><a data-toggle="tab" href="#a3"><b>Pekerjaan</b></a></li>
                    <li><a data-toggle="tab" href="#a4"><b>Referensi</b></a></li>
                    <li><a data-toggle="tab" href="#a5"><b>Tabungan</b></a></li>
                    <li><a data-toggle="tab" href="#a6"><b>Minat</b></a></li>
                    <li><a data-toggle="tab" href="#a7"><b>Dokumen</b></a></li>
                <div class="tab-content" style="margin-top: 15px;">
                    <div id="diri" class="tab-pane fade active in">
                        <fieldset style="padding-top: 20px; padding-bottom: 10px">

                            <div class="panel-12" style="text-align: left;"><label>Nama Lengkap *)</label></div>
                            <input type="text" id="nama" name="nama" style="text-transform:uppercase" disabled="true" />
                            <div class="panel-12" style="text-align: left;"><label>Nama Panggilan *)</label></div>
                            <input type="text" id="panggilan" style="text-transform:uppercase" />
                            <div class="panel-12">
                                <div class="panel-8">
                                    <div class="panel-12" style="text-align: left;"><label>Tempat Lahir *)</label></div>
                                    <input type="text" id="tempat_lahir" style="text-transform:uppercase" />
                                </div>
                                <div class="panel-1">
                                    &nbsp;
                                </div>

                                <div class="panel-3">
                                    <div class="panel-12" style="text-align: left;"><label>Tanggal Lahir *)</label></div>
                                    <input type="text" id="tanggal_lahir" placeholder="DD/MM/YYYY" id="datepicker" />
                                </div>
                            </div>
                            <div class="panel-12" style="text-align: left;"><label>No KTP *)</label></div>
                            <input type="number" id="ktp" />
                            <div class="panel-12" style="text-align: left;"><label>Hobby / Kegemaran </label></div>
                            <input type="text" id="hobi" style="text-transform:uppercase" />
                        </fieldset>
                    </div>

                    <div id="a1" class="tab-pane fade">
                        <fieldset style="padding-top: 20px; padding-bottom: 10px">
                            <div class="panel-12" style="text-align: left;"><label>Nomor Handphone *)</label></div>
                            <input type="number" id="hp" />
                            <div class="panel-12" style="text-align: left;"><label>Nomor WhatsApp (WA)</label></div>
                            <input type="number" id="wa"  />
                            <div class="panel-12">
                                <div class="panel-1" style="text-align: left;">
                                    <input type="checkbox" id="cek_wa" value="cek_wa">
                                </div>
                                <div class="panel-11" style="text-align: left;">Ditambahkan pada Group WA YPS</div>

                            </div>
                            <div class="panel-12" style="text-align: left;"><label>Email Pribadi</label></div>
                            <input type="email" id="email" />
                            <div class="panel-12" style="text-align: left;"><label>PIN BBM</label></div>
                            <input type="text" id="bbm" />
                            <div class="panel-12" style="text-align: left;"><label>Facebook</label></div>
                            <input type="url" id="facebook"  />
                        </fieldset>                </div>

                    <div id="a2" class="tab-pane fade">
                        <fieldset style="padding-top: 20px; padding-bottom: 10px">
                            <div class="panel-12" style="text-align: left;"><label>Alamat Rumah *)</label></div>
                            <textarea id="alamat1" style="text-transform:uppercase" ></textarea>
                            <div class="panel-12" style="text-align: left;"><label>RT/RW Kelurahan *)</label></div>
                            <input type="text" style="text-transform:uppercase" id="alamat2" />
                        </fieldset>
                    </div>


                    <div id="a3" class="tab-pane fade">
                        <fieldset style="padding-top: 20px; padding-bottom: 10px">
                            <div class="panel-12" style="text-align: left;"><label>Pekerjaan *)</label></div>
                            <input type="text" id="pekerjaan" id="pekerjaan" style="text-transform:uppercase" />
                            <!-- <input type="text" id="pekerjaan" style="text-transform:uppercase" /> -->
                            <div class="panel-12" style="text-align: left;"><label>Nama Instansi/Sekolah *)</label></div>
                            <input type="text" id="bidang" style="text-transform:uppercase" />
                            <div class="panel-12" style="text-align: left;"><label>Jabatan dalam Perusahaan *)</label></div>
                            <input type="text" id="jabatan" style="text-transform:uppercase" />
                        </fieldset>
                    </div>

                    <div id="a4" class="tab-pane fade">
                        <fieldset style="padding-top: 20px; padding-bottom: 10px">

                            <div class="panel-12" style="text-align: left;"><label>Nama Paroki *)</label></div>
                            <input type="text" id="paroki" class="form-control" />
                            <!-- <input type="text" id="paroki" style="text-transform:uppercase"  /> -->
                            <div class="panel-12" style="text-align: left;"><label>ID Anggota Pemberi Referensi *)</label></div>
                            <input type="number" id="id_referensi" />
                            <div class="panel-12" style="text-align: left;"><label>Nama Pemberi Referensi *)</label></div>
                            <input type="text" id="nama_referensi" style="text-transform:uppercase"  />
                            <div class="panel-12" style="text-align: left;"><label>No. HP Pemberi Referensi *)</label></div>
                            <input type="number" id="hp_referensi" />
                        </fieldset>
                    </div>
                    <div id="a5" class="tab-pane fade">
                        <fieldset style="padding-top: 20px; padding-bottom: 10px">

                            <div class="panel-12" style="text-align: left;"><label>Nama Tabungan *)</label></div>
                            <input type="text" id="tanggal_selesai" class="form-control" />
                            <div class="panel-12" style="text-align: left;"><label>Tanggal Mulai Iuran Tabungan *)</label></div>
                            <input type="text" placeholder="DD/MM/YYYY" id="tanggal_mulai" />
                            <div class="panel-12" style="text-align: left;"><label>Besaran Iuran Tabungan *)</label></div>
                            <select id="iuran" class="form-control" style="margin-bottom: 15px;" >
                                <option value="200000">&nbsp;&nbsp;Rp. 200.000 / bulan&nbsp;&nbsp;</option>
                                <option value="500000">&nbsp;&nbsp;Rp. 500.000 / bulan&nbsp;&nbsp;</option>
                                <option value="1000000">&nbsp;&nbsp;Rp. 1.000.000 / bulan&nbsp;&nbsp;</option>

                            </select>
                            <div class="panel-12" style="text-align: left;"><label>Metode Transfer *)</label></div>
                            <select id="tbb" class="form-control" style="margin-bottom: 15px;">
                                <option value="TRANSFER" selected>&nbsp;&nbsp;Transfer ke Rekening BCA YPS</option>
                                <option value="DEBET">&nbsp;&nbsp;Auto Debet dari Rekening BCA Anda&nbsp;&nbsp;</option>
                            </select>
                        </fieldset>
                    </div>
                    <div id="a6" class="tab-pane fade">
                        <fieldset style="padding-top: 20px; padding-bottom: 10px">
                            <div class="panel-12" style="text-align: left;"><label>Interest/Minat</label></div>
                            <div class="panel-12">
                                <div class="panel-4">
                                    <div class="funkyradio">
                                        <div class="funkyradio-success">
                                            <input type="checkbox" id="minat01"/>
                                            <label for="minat01">Property</label>
                                        </div>  
                                        <div class="funkyradio-success">
                                            <input type="checkbox" id="minat02"/>
                                            <label for="minat02">Perbankan</label>
                                        </div> 
                                        <div class="funkyradio-success">
                                            <input type="checkbox" id="minat03"/>
                                            <label for="minat03">Keuangan</label>
                                        </div> 
                                        <div class="funkyradio-success">
                                            <input type="checkbox" id="minat04"/>
                                            <label for="minat04">Konsultan</label>
                                        </div> 
                                    </div>
                                </div>
                                <div class="panel-4">
                                    <div class="funkyradio">
                                        <div class="funkyradio-success">
                                            <input type="checkbox" id="minat05"/>
                                            <label for="minat05">Pendidikan/Training</label>
                                        </div>
                                        <div class="funkyradio-success">
                                            <input type="checkbox" id="minat06"/>
                                            <label for="minat06">Konstruksi</label>
                                        </div> 
                                        <div class="funkyradio-success">
                                            <input type="checkbox" id="minat07"/>
                                            <label for="minat07">Kuliner/makanan</label>
                                        </div> 
                                        <div class="funkyradio-success">
                                            <input type="checkbox" id="minat08"/>
                                            <label for="minat08">Kontraktor</label>
                                        </div>                         
                                    </div>
                                </div>
                                <div class="panel-4">
                                    <div class="funkyradio">
                                        <div class="funkyradio-success">
                                            <input type="checkbox" id="minat09"/>
                                            <label for="minat09">Otomotif</label>
                                        </div>
                                        <div class="funkyradio-success">
                                            <input type="checkbox" id="minat10"/>
                                            <label for="minat10">Transportasi</label>
                                        </div> 
                                        <div class="funkyradio-success">
                                            <input type="checkbox" id="minat11"/>
                                            <label for="minat11">Electricity</label>
                                        </div> 
                                        <div class="funkyradio-success">
                                            <input type="checkbox" id="minat12"/>
                                            <label for="minat12">Elektronics</label>
                                        </div>                         
                                    </div>
                                </div>
                            </div>
                            <div class="panel-12" style="text-align: left;"><label>Motivasi mengikuti TBB</label></div>
                            <div class="panel-12">
                                <div class="panel-6">
                                    <div class="funkyradio">
                                        <div class="funkyradio-success">
                                            <input type="checkbox" id="motivasi01"/>
                                            <label for="motivasi01">Menambah Wawasan</label>
                                        </div>  
                                        <div class="funkyradio-success">
                                            <input type="checkbox" id="motivasi02"/>
                                            <label for="motivasi02">Sebagai Anggota Biasa</label>
                                        </div> 
                                        <div class="funkyradio-success">
                                            <input type="checkbox" id="motivasi03"/>
                                            <label for="motivasi03">Menyalurkan Berkat</label>
                                        </div> 
                                        <div class="funkyradio-success">
                                            <input type="checkbox" id="motivasi04"/>
                                            <label for="motivasi04">Memberikan Pendampingan</label>
                                        </div> 
                                        <div class="funkyradio-success">
                                            <input type="checkbox" id="motivasi05"/>
                                            <label for="motivasi05">Memberikan training</label>
                                        </div> 
                                    </div>
                                </div>
                                <div class="panel-6">
                                    <div class="funkyradio">
                                        <div class="funkyradio-success">
                                            <input type="checkbox" id="motivasi06"/>
                                            <label for="motivasi06">Memiliki Komunitas & Kegiatan Positif</label>
                                        </div>
                                        <div class="funkyradio-success">
                                            <input type="checkbox" id="motivasi07"/>
                                            <label for="motivasi07">Ikut berperan dalam kegiatan Pukat</label>
                                        </div> 
                                        <div class="funkyradio-success">
                                            <input type="checkbox" id="motivasi08"/>
                                            <label for="motivasi08">Memberikan Pelayanan</label>
                                        </div> 
                                        <div class="funkyradio-success">
                                            <input type="checkbox" id="motivasi09"/>
                                            <label for="motivasi09">Sharing Pengalaman</label>
                                        </div>                         
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div id="a7" class="tab-pane fade">
                          <div class="panel-12">
                            <h4>KTP</h4>
                            <img src="ktp.jsp?kode=<%=userid%>" alt="" height="300px" onerror="this.src='img/foto.gif'"/>
                            <a href="ktp.jsp?kode=<%=userid%>" target="_blank" class="btn btn-success"><h4>Zoom</h4></a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <script src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
        <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src='//cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.min.js'></script>
        <script src='//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>


        <script>
            <%=hasil%>
        </script>
    </body>
</html>

