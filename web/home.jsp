<%@page import="org.smi.komponen"%>
<%@page import="org.smi.getDataGrafik"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.smi.koneksiDatabase"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    session.setAttribute("halaman", "home.jsp");
    session.setAttribute("judul", "HALAMAN DEPAN");

    koneksiDatabase kb = new koneksiDatabase();
    Connection kon = kb.getKoneksiSumber();
    komponen ko = new komponen();
    String anggota = kb.getStringSQL(kon, "SELECT count(1) as jml FROM pukat_pihak WHERE isMember = true");
    int masuk = kb.getIntSQL(kon, "SELECT sum(masuk) as jml FROM pukat_tabungan_pihak");
    int keluar = 0;
    int saldo = masuk - keluar;
    String masuks = ko.ribuan(masuk);
    String keluars = ko.ribuan(keluar);
    String saldos = ko.ribuan(saldo);

    getDataGrafik gd = new getDataGrafik();
    String tbl_masuk = gd.getDataTabelGrafik(kon, "SELECT UPPER(nama) as nama, IFNULL(hp, '-') as hp FROM pukat_pihak WHERE isMember = 1 ORDER BY id DESC LIMIT 10");
 String tbl_app = gd.getDataTabelGrafik(kon, "SELECT UPPER(nama) as nama, IFNULL(hp, '-') as hp FROM pukat_pihak WHERE isMember = 2 ORDER BY id DESC LIMIT 10");

    kon.close();
%>
<div class="penel-12" style="margin-top: 15px;">    
    <div class="col-lg-3 col-xs-12">
        <div class="card">
            <div class="card-header" data-background-color="blue">
                <h4 class="title">JUMLAH ANGGOTA</h4>
            </div>
            <div class="card-content card-chart">
                <div class="ct-chart" style="height:50px; text-align: center;">
                    <div class="text-biru-tua-tebal"><%=anggota%> Anggota</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-xs-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">TOTAL PEMASUKAN</h4>
            </div>
            <div class="card-content card-chart">
                <div class="ct-chart" style="height:50px; text-align: center;">
                    <div class="text-biru-tua-tebal"><%=masuks%></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-xs-12">
        <div class="card">
            <div class="card-header" data-background-color="red">
                <h4 class="title">TOTAL PENGELUARAN</h4>
            </div>
            <div class="card-content card-chart">
                <div class="ct-chart" style="height:50px; text-align: center;">
                    <div class="text-biru-tua-tebal"><%=keluars%></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-xs-12">
        <div class="card">
            <div class="card-header" data-background-color="orange">
                <h4 class="title">SALDO AKHIR</h4>
            </div>
            <div class="card-content card-chart">
                <div class="ct-chart" style="height:50px; text-align: center;">
                    <div class="text-biru-tua-tebal"><%=saldos%></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel-12">
    <div class="col-lg-7 col-xs-12">
        <div class="card">
            <div class="card-header" data-background-color="green">
                <h4 class="title">GRAFIK PEMASUKAN TABUNGAN</h4>
            </div>
            <div class="card-content card-chart">
                <div class="ct-chart" style="height:400px; text-align: center;">
                    <div class="ct-chart" id="masuk_chart" style="height:400px"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-5 col-xs-12">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header" data-background-color="red">
                    <h4 class="title">Anggota belum disetujui</h4>
                </div>
                <div class="card-content table-responsive">
                    <table class="table table-hover">
                        <thead class="text-warning">
                            <tr>

                            </tr> 
                        </thead>
                        <tbody>
                            <%=tbl_app%>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header" data-background-color="blue">
                    <h4 class="title">10 Anggota Terbaru</h4>
                </div>
                <div class="card-content table-responsive">
                    <table class="table table-hover">
                        <thead class="text-warning">
                            <tr>

                            </tr> 
                        </thead>
                        <tbody>
                            <%=tbl_masuk%>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-header-skp">DAILY REPORT  > DATA SISWA
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <br />
        </div>
        <iframe width="100%" height="80%" frameborder="0" allowtransparency="true" id="framekukoreksi1"></iframe>  
    </div>
</div>
<script type="text/javascript">

    getDefaultTargetAndRevenue('lib/grafikMasuk.jsp', '#masuk_chart', '');
    function getDefaultTargetAndRevenue(url, id, yTitle) {

        $.ajax({
            type: "GET",
            url: url,
            dataType: "json"
        }).done(function (data) {
            var i = 0;
            var arrBln = new Array();
            var target = new Array();

            //    var yTitle = "DALAM RIBUAN RUPIAH";
            $.each(data, function (i, item) {
                arrBln[i] = item.periode;

                target[i] = parseInt(item.jumlah);

                i++;
            })
            grafikLine(arrBln, target, yTitle, id);
        });

    }

    function grafikLine(bulan, target, yTitle, id) {

        $(function () {
            $(id).highcharts({
                chart: {type: 'line'},
                exporting: {enabled: false},
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                subtitle: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                }, credits: {
                    enabled: false
                },
                xAxis:
                        {
                            categories: bulan,
                            labels:
                                    {
                                        enabled: false

                                    },
                            title: {text: 'BULAN'}
                        },
                yAxis: {title: {text: yTitle},
                    labels:
                            {
                                enabled: true

                            }},
                tooltip: {formatter: function () {
                        var s = '<b>' + toBulan(this.x) + '</b>';

                        $.each(this.points, function () {
                            s += '<br/>' + this.series.name + ': <b>' + toRp(this.y) + '</b>';
                        });

                        return s;
                    },
                    shared: true},
                plotOptions: {
                    area: {
                        fillColor: {
                            linearGradient: {
                                x1: 0,
                                y1: 0,
                                x2: 0,
                                y2: 1
                            },
                            stops: [
                                [0, Highcharts.getOptions().colors[0]],
                                [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                            ]
                        },
                        marker: {
                            radius: 2
                        },
                        lineWidth: 2,
                        states: {
                            hover: {
                                lineWidth: 1
                            }
                        },
                        threshold: null
                    }
                },
                series: [{
                        showInLegend: false,
                        name: "Jumlah",
                        data: target,
                        type: 'area',
                        lineWidth: 5, states: {
                            hover: {
                                lineWidth: 5
                            }
                        },
                        marker: {
                            enabled: false
                        },
                        zIndex: 2,
                        point: {
                        }
                    }]
            });
        });
    }
    function toRp(angka) {
        var rev = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2 = '';
        for (var i = 0; i < rev.length; i++) {
            rev2 += rev[i];
            if ((i + 1) % 3 === 0 && i !== (rev.length - 1)) {
                rev2 += '.';
            }
        }
        return rev2.split('').reverse().join('');
    }
    function toBulan(angka) {
        angka = angka.replace('-', '');
        var thn = angka.substring(0, 4);
        var bln = angka.substring(4, 6);
        var tgl = '';
        if (angka.length == 8) {
            tgl = angka.substring(6, 8) + ' ';
        }
        if (bln == '01') {
            bln = 'Januari';
        } else if (bln == '02') {
            bln = 'Februari';
        } else if (bln == '03') {
            bln = 'Maret';
        } else if (bln == '04') {
            bln = 'April';
        } else if (bln == '05') {
            bln = 'Mei';
        } else if (bln == '06') {
            bln = 'Juni';
        } else if (bln == '07') {
            bln = 'Juli';
        } else if (bln == '08') {
            bln = 'Agustus';
        } else if (bln == '09') {
            bln = 'September';
        } else if (bln == '10') {
            bln = 'Oktober';
        } else if (bln == '11') {
            bln = 'November';
        } else if (bln == '12') {
            bln = 'Desember';
        }

        return tgl + bln + ' ' + thn;
    }
</script>
