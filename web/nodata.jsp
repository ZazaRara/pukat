<%@page import="org.aruskas.simpanData"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.Map"%>
<%
    Object user = session.getAttribute("punyaku");
    Map<String, String[]> parameters = request.getParameterMap();
    simpanData sp = new simpanData();
    try {
        boolean bh = sp.simpanApproval(parameters, user.toString());
        if (bh) {
            session.setAttribute("halaman", "approveMasuk.jsp");
            response.sendRedirect("index.jsp");
        }
    } catch (Exception e) {
        response.sendRedirect("redirect.jsp");
    }
%>