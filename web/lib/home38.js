var sm = (function () {


    function getHTML(id, url) {

        $('#' + id).empty();
        $('#' + id).append("<div style='text-align:center;'><img src='img/loading.gif' height='200px' /><br /><strong>Sedang memuat...</strong></div>");

        $.get(url, function (data) {
            $('#' + id).empty();
            $('#' + id).html(data);
            // alert("Load was performed.");
        });
    }
    this.jadwalsaya = function () {
        $('#judulatas').empty();
        $('#judulatas').append('JADWAL SAYA');
        getHTML('dataUtama', 'jadwalSaya.jsp');
    }

    this.siswasaya = function () {
        $('#judulatas').empty();
        $('#judulatas').append('SISWA SAYA');
        getHTML('dataUtama', 'siswaSaya.jsp');

    }
    this.datapengajar = function () {
        $('#judulatas').empty();
        $('#judulatas').append('DATA ANGGOTA DIBLOK');
        getHTML('dataUtama', 'anggotaDataBlok.jsp');

    }
    this.pendidikansaya = function () {
        $('#judulatas').empty();
        $('#judulatas').append('KALENDER PENDIDIKAN');
        getHTML('dataUtama', 'kalendarPendidikan.jsp');

    }
     this.tagihan = function () {
        $('#judulatas').empty();
        $('#judulatas').append('DATA ANGGOTA BELUM APPROVE');
        getHTML('dataUtama', 'anggotaDataPending.jsp');

    }
    this.datasaya = function () {
        $('#judulatas').empty();
        $('#judulatas').append('DATA SAYA');
        getHTML('dataUtama', 'dataSaya.jsp');

    }
    this.datahome = function () {
        $('#judulatas').empty();
        $('#judulatas').append('HALAMAN DEPAN');
        getHTML('dataUtama', 'home.jsp');

    }

    this.datahomecso = function () {
        $('#judulatas').empty();
        $('#judulatas').append('HALAMAN DEPAN');
        getHTML('dataUtama', 'home-cso.jsp');

    }
    this.jadwalguru = function () {
        $('#judulatas').empty();
        $('#judulatas').append('JADWAL PENGAJAR');
        getHTML('dataUtama', 'dataJadwalPengajar.jsp');

    }
    this.dashboardKesiswaan = function () {
        $('#judulatas').empty();
        $('#judulatas').append('DATA ANGGOTA');
        getHTML('dataUtama', 'anggotaData.jsp');

    }
    this.dailyreport = function () {
        $('#judulatas').empty();
        $('#judulatas').append('DAILY REPORT SISWA');
        getHTML('dataUtama', 'dailyReport.jsp');

    }
    this.dashboardKeuangan = function () {
        $('#judulatas').empty();
        $('#judulatas').append('DASHBOARD DATA KEUANGAN');
        getHTML('dataUtama', 'dashboardKeuangan.jsp');

    }
    this.budget = function () {
        $('#judulatas').empty();
        $('#judulatas').append('KONTROL PENGELUARAN / BIAYA');
        getHTML('dataUtama', 'keuanganBiaya.jsp');

    }
    this.evaluasi = function () {
        $('#judulatas').empty();
        $('#judulatas').append('EVALUASI SISWA');
        getHTML('dataUtama', 'siswaEvaluasi.jsp');

    }
    this.grafikLine = function (bulan, target, yTitle, id) {

        $(function () {
            $(id).highcharts({
                chart: {type: 'line'},
                exporting: {enabled: false},
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                subtitle: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                }, credits: {
                    enabled: false
                },
                xAxis:
                        {
                            categories: bulan,
                            labels:
                                    {
                                        enabled: false

                                    },
                            title: {text: 'BULAN'}
                        },
                yAxis: {title: {text: yTitle},
                    labels:
                            {
                                enabled: false

                            }},
                tooltip: {formatter: function () {
                        var s = '<b>' + toBulan(this.x) + '</b>';

                        $.each(this.points, function () {
                            s += '<br/>' + this.series.name + ': <b>' + toRp(this.y) + '</b>';
                        });

                        return s;
                    },
                    shared: true},
                plotOptions: {
                    area: {
                        fillColor: {
                            linearGradient: {
                                x1: 0,
                                y1: 0,
                                x2: 0,
                                y2: 1
                            },
                            stops: [
                                [0, Highcharts.getOptions().colors[0]],
                                [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                            ]
                        },
                        marker: {
                            radius: 2
                        },
                        lineWidth: 2,
                        states: {
                            hover: {
                                lineWidth: 1
                            }
                        },
                        threshold: null
                    }
                },
                series: [{
                        showInLegend: false,
                        name: "Jumlah",
                        data: target,
                        type: 'area',
                        lineWidth: 5, states: {
                            hover: {
                                lineWidth: 5
                            }
                        },
                        marker: {
                            enabled: false
                        },
                        zIndex: 2,
                        point: {
                            events: {
                                click: function (event) {
                                    var a = 'showDetailTabel.jsp?b=' + this.category + '&c=' + yTitle;
                                    if (yTitle == 'Jumlah Siswa Baru') {
                                        a = a + '&a=SISWA_BARU';
                                    } else if (yTitle == 'Jumlah Siswa Cuti') {
                                        a = a + '&a=SISWA_CUTI';
                                    } else if (yTitle == 'Jumlah Siswa Keluar') {
                                        a = a + '&a=SISWA_KELUAR';
                                    } else if (yTitle == 'Jumlah Pemasukan') {
                                        a = a + '&a=PEMASUKAN';
                                    } else if (yTitle == 'Jumlah Pengeluaran') {
                                        a = a + '&a=PENGELUARAN';
                                    } else if (yTitle == 'Jumlah Laba') {
                                        a = a + '&a=LABA';
                                    }

                                    $("#urlweb").attr('src', a);
                                    $("#detail").modal();
                                }
                            }
                        }
                    }]
            });
        });
    }
    this.grafikLineNoTitle = function (bulan, target, yTitle, id) {

        $(function () {
            $(id).highcharts({
                chart: {type: 'line'},
                exporting: {enabled: false},
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    },
                    enabled: false
                },
                subtitle: {
                    text: '',
                    style: {
                        display: 'none'
                    },
                    enabled: false
                }, credits: {
                    enabled: false
                },
                xAxis:
                        {
                            categories: bulan,
                            labels:
                                    {
                                        enabled: false

                                    },
                            title: {text: 'BULAN',  enabled: false},
                        },
                yAxis: {title: {text: yTitle,  enabled: false},
                    labels:
                            {
                                enabled: false

                            }},
                tooltip: {formatter: function () {
                        var s = '<b>' + toBulan(this.x) + '</b>';

                        $.each(this.points, function () {
                            s += '<br/>' + this.series.name + ': <b>' + toRp(this.y) + '</b>';
                        });

                        return s;
                    },
                    shared: true},
                plotOptions: {
                    area: {
                        fillColor: {
                            linearGradient: {
                                x1: 0,
                                y1: 0,
                                x2: 0,
                                y2: 1
                            },
                            stops: [
                                [0, Highcharts.getOptions().colors[0]],
                                [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                            ]
                        },
                        marker: {
                            radius: 2
                        },
                        lineWidth: 2,
                        states: {
                            hover: {
                                lineWidth: 1
                            }
                        },
                        threshold: null
                    }
                },
                series: [{
                        showInLegend: false,
                        name: "Jumlah",
                        data: target,
                        type: 'area',
                        lineWidth: 5, states: {
                            hover: {
                                lineWidth: 5
                            }
                        },
                        marker: {
                            enabled: false
                        },
                        zIndex: 2,
                        point: {
                            events: {
                                click: function (event) {
                                    var a = 'showDetailTabel.jsp?b=' + this.category + '&c=' + yTitle;
                                    if (yTitle == 'Jumlah Siswa Baru') {
                                        a = a + '&a=SISWA_BARU';
                                    } else if (yTitle == 'Jumlah Siswa Cuti') {
                                        a = a + '&a=SISWA_CUTI';
                                    } else if (yTitle == 'Jumlah Siswa Keluar') {
                                        a = a + '&a=SISWA_KELUAR';
                                    } else if (yTitle == 'Jumlah Siswa Batal') {
                                        a = a + '&a=SISWA_BATAL';
                                    } else if (yTitle == 'Jumlah Siswa Trial') {
                                        a = a + '&a=SISWA_TRIAL';
                                    } else if (yTitle == 'Jumlah Pemasukan') {
                                        a = a + '&a=PEMASUKAN';
                                    } else if (yTitle == 'Jumlah Pengeluaran') {
                                        a = a + '&a=PENGELUARAN';
                                    } else if (yTitle == 'Jumlah Laba') {
                                        a = a + '&a=LABA';
                                    }

                                    $("#urlweb").attr('src', a);
                                    $("#detail").modal();
                                }
                            }
                        }
                    }]
            });
        });
    }
    this.tampilTabelSiswaSaya = function (id, q) {


        $("#" + id).empty();
        $('#' + id).append("<img src='img/loading.gif' height='100px' /><br /><strong>Sedang memuat...</strong>");
        $.ajax({
            type: "GET",
            url: "lib/listSiswaSaya.jsp?q=" + q,
            dataType: "json",
        }).done(function (data) {
            $("#" + id).empty();
            if (data.length == 0) {
                $('#' + id).append("DATA TIDAK DITEMUKAN");
            } else {



                $.each(data, function (data, n) {
                    var ss = n.sta;
                    var warna = "abu";
                    var staku = '<input class="btn btn-success" id="rekap3' + n.id + '" value="WAITING" disabled />';
                    if (ss == 'AKTIF') {
                        staku = '<input class="btn btn-primary" id="rekap3' + n.id + '" value="AKTIF" disabled />';
                        warna = "biru-skp";
                    } else if (ss == 'KELUAR') {
                        staku = '<input class="btn btn-danger" id="rekap3' + n.id + '" value="KELUAR" disabled />';
                        warna = "merah";
                    } else if (ss == 'CUTI') {
                        staku = '<input class="btn btn-warning" id="rekap3' + n.id + '" value="WAITING" disabled />';
                        warna = "oranye";
                    }
                    var link = '<a href="#" data-toggle="modal" id="edit' + n.id + '"><span class="fa fa-edit fa-lg"></span></a>';
                    if (n.instrumen == 'ERROR HUB' || n.instrumen == 'MTL' || n.instrumen == 'FOM & IMC' || n.instrumen == 'GROUP') {
                        link = '';
                    }
                    var xx = '<div class="circle-tile panel-foto-staff ' + warna + '" data-id="' + n.id
                            + '"><div class="circle-tile-footer"><div class="text-hitam-tebal tebal weks"><u>' + n.nama
                            + '</u><br /><i class="jabatan-staff">' + n.nis
                            + '</i></div></div><a href="#" id="rekap' + n.id + '"><img src="foto.jsp?kode=' + n.id
                            + '" alt="" class="gambar-staff-tengah" onerror="this.src=\'img/foto.gif\'"><div class="text-biru-tua-tebal">' + n.panggilan
                            + '</div></a>'
                            + staku
                            + '<br><div class="text-hitam-tebal tebal weks">' + n.program + ' - ' + n.instrumen + ''
                            + '<br>' + n.grade + ' / ' + n.semester + '&nbsp; &nbsp; &nbsp; ' + link + '</div>'
                            + '</div>';
                    $('#' + id).append(
                            xx
                            );

                    $('#rekap' + n.id).click(function (e) {
                        $("#framekukoreksi").attr('src', 'detailSiswaSaya.jsp?kode=' + n.id + '&nama=' + n.nama + '&nis=' + n.nis + '&nohp=' + n.no_hp + '&panggil=' + n.panggilan);
                        $("#rekap").modal();
                    });
                    $('#rekap3' + n.id).click(function (e) {
                        $("#framekukoreksi").attr('src', 'detailSiswaSaya.jsp?kode=' + n.id + '&nama=' + n.nama + '&nis=' + n.nis + '&nohp=' + n.no_hp + '&panggil=' + n.panggilan);
                        $("#rekap").modal();
                    });
                    $('#edit' + n.id).click(function (e) {
                        $("#framekuedit").attr('src', 'siswaInputGrade.jsp?a=' + n.gradeid + '&b=' + n.nama + '&c=' + n.instrumen + '&d=' + n.grade + '&e=' + n.semester);
                        $("#edit").modal();
                    });
                });



            }


        }).fail(function () {
            $("#" + id).empty();
            $('#' + id).append("<tr><td colspan='6'>ERROR LOADING</td></tr>");
            //      alert('Error Loading');
        });
    }
    this.tampilTabelSiswaSayaDetailJadwal = function (id, q, per, tgl) {

        $("#" + id).empty();
        $('#' + id).append("<img src='img/loading.gif' height='100px' /><br /><strong>Sedang memuat...</strong>");
        $.ajax({
            type: "GET",
            url: "lib/listSiswaSayaDetailJadwal.jsp?q=" + q + "&per=" + per,
            dataType: "json",
        }).done(function (data) {
            $("#" + id).empty();
            if (data.length == 0) {
                $('#' + id).append("DATA TIDAK DITEMUKAN");
            } else {

                $.each(data, function (data, n) {
                    var inp = '<input class="btn btn-primary" value="SUDAT TERINPUT" disabled /><a href="#" data-toggle="modal" id="editDaily' + n.no + '"><span class="fa fa-edit fa-lg"></span></a>';

                    var war = 'biru-skp';
                    if (n.repot == 'KOSONG') {
                        inp = '<a href="#" data-toggle="modal" id="input' + n.no + '" class="btn btn-success">INPUT DAILY REPORT</a>';
                        war = 'abu';
                    }
                    var ss = n.sta;
                    var staku = '<input class="btn btn-success" style="height:20px;width:20px;" readonly />';
                    if (ss == 'AKTIF') {
                        staku = '<input class="btn btn-primary" style="height:20px;width:20px;" readonly />';
                    } else if (ss == 'KELUAR') {
                        staku = '<input class="btn btn-danger" style="height:20px;width:20px;" readonly />';
                    } else if (ss == 'CUTI') {
                        staku = '<input class="btn btn-warning" style="height:20px;width:20px;" readonly />';
                    }
                    var link = '<a href="#" data-toggle="modal" id="edit' + n.id + '"><span class="glyphicon glyphicon-edit"></span></a>';
                    if (n.instrumen == 'ERROR HUB') {
                        link = '';
                    }
                    var xx = '<div class="circle-tile panel-foto-staff ' + war + '" data-id="' + n.id
                            + '"><div class="circle-tile-footer"><div class="text-hitam-tebal tebal weks"><u>' + n.nama
                            + '</u><br /><i class="jabatan-staff">' + n.nis
                            + '</i></div></div><a href="#" id="rekap' + n.id + '"><img src="foto.jsp?kode=' + n.id
                            + '" alt="" class="gambar-staff-tengah" onerror="this.src=\'img/foto.gif\'"><div class="text-biru-tua-tebal">' + n.panggilan
                            + '</div></a>'
                            + '<div class="text-hitam-tebal tebal weks">' + n.program + ' - ' + n.instrumen + ''
                            + '<br />' + n.jenis + ' / ' + n.semester + '&nbsp; &nbsp; &nbsp; ' + link + '</div><br />'
                            + inp
                            + '</div>';
                    $('#' + id).append(
                            xx
                            );

                    $('#rekap' + n.id).click(function (e) {
                        $("#framekukoreksi").attr('src', 'detailSiswaSaya.jsp?kode=' + n.id + '&nama=' + n.nama + '&nis=' + n.nis + '&nohp=' + n.no_hp + '&panggil=' + n.panggilan);
                        $("#rekap").modal({backdrop: "static"});
                    });
                    $('#edit' + n.id).click(function (e) {
                        $("#framekuedit").attr('src', 'siswaInputGrade.jsp?a=' + n.gradeid + '&b=' + n.nama + '&c=' + n.instrumen + '&d=' + n.jenis + '&e=' + n.semester);
                        $("#edit").modal();
                    });
                    $('#input' + n.no).click(function (e) {
                        $("#framekukoreksiinput").attr('src', 'dailyReportInput.jsp?a=' + n.id
                                + '&b=' + n.nama + '&c=' + n.program + ' - ' + n.instrumen
                                + '&d=' + tgl + '&e=' + q + '&f=' + per);
                        $("#inputdata").modal();
                    });
                      $('#editDaily' + n.no).click(function (e) {
                        $("#framekuedit").attr('src', 'dailyReportEdit.jsp?a=' + n.id_pihak
                                + '&b=' + n.nama + '&c=' + n.program + ' - ' + n.instrumen
                                + '&d=' + tgl + '&e=' + n.id_jadwal + '&f=' + n.pertemuan+ '&idr=' + n.repot+ '&g=' + n.catatan+ '&h=' + n.penilaian);;
                        $("#edit").modal();
                    });
                });



            }


        }).fail(function () {
            $("#" + id).empty();
            $('#' + id).append("<tr><td colspan='6'>ERROR LOADING</td></tr>");
            //      alert('Error Loading');
        });
    }
    this.detailSiswaSayaRiwayatKelas = function (id, q) {


        $("#" + id).empty();
        $('#' + id).append("<tr><td colspan='6'><img src='img/loading.gif' height='100px' /><br /><strong>Sedang memuat...</strong></td></tr>");
        $.ajax({
            type: "GET",
            url: "lib/riwayatKelasSiswa.jsp?q=" + q,
            dataType: "json",
        }).done(function (data) {
            $("#" + id).empty();
            if (data.length == 0) {
                $('#' + id).append("<tr><td colspan='6'>DATA TIDAK DITEMUKAN</td></tr>");
            } else {

                $.each(data, function (data, n) {
                    $('#' + id).append(
                            '<tr role="row" class="abu-muda" data-id="' + n.id + '">' +
                            '<td style="text-align: center">' + n.no +
                            '</td><td style="text-align: left">' + n.nama +
                            '</td><td style="text-align: left">' + n.program +
                            '</td><td style="text-align: center">' + n.status +
                            '</td><td style="text-align: center">' + n.tgl +
                            '</td><td style="text-align: center">' + n.tgl_akhir +
                            '</td></tr>'
                            );


                });



            }


        }).fail(function () {
            $("#" + id).empty();
            $('#' + id).append("<tr><td colspan='6'>ERROR LOADING</td></tr>");
            //      alert('Error Loading');
        });
    }
    this.detailSiswaSayaRiwayatReport = function (id, q) {


        $("#" + id).empty();
        $('#' + id).append("<tr><td colspan='6'><img src='img/loading.gif' height='100px' /><br /><strong>Sedang memuat...</strong></td></tr>");
        $.ajax({
            type: "GET",
            url: "lib/riwayatReportSiswa.jsp?q=" + q,
            dataType: "json",
        }).done(function (data) {
            $("#" + id).empty();
            if (data.length == 0) {
                $('#' + id).append("<tr><td colspan='6'>DATA TIDAK DITEMUKAN</td></tr>");
            } else {

                $.each(data, function (data, n) {
                    $('#' + id).append(
                            '<tr role="row" class="abu-muda">' +
                            '<td style="text-align: center">' + n.no +
                            '</td><td style="text-align: center">' + n.tanggal +
                            '</td><td style="text-align: left">' + n.program + ' - ' + n.jenis +
                            '</td><td style="text-align: center">' + n.nama +
                            '</td><td style="text-align: left">' + n.penilaian +
                            '</td><td style="text-align: left">' + n.catatan +
                            '</td><td style="text-align: left">' + n.pengumuman +
                            '</td></tr>'
                            );


                });



            }


        }).fail(function () {
            $("#" + id).empty();
            $('#' + id).append("<tr><td colspan='6'>ERROR LOADING</td></tr>");
            //      alert('Error Loading');
        });
    }
    this.detailSiswaSayaPresensi = function (id, q) {


        $("#" + id).empty();
        $('#' + id).append("<tr><td colspan='6'><img src='img/loading.gif' height='100px' /><br /><strong>Sedang memuat...</strong></td></tr>");
        $.ajax({
            type: "GET",
            url: "lib/riwayatKelasSiswa.jsp?q=" + q,
            dataType: "json",
        }).done(function (data) {
            $("#" + id).empty();
            if (data.length == 0) {
                $('#' + id).append("<tr><td colspan='6'>DATA TIDAK DITEMUKAN</td></tr>");
            } else {

                $.each(data, function (data, n) {
                    $('#' + id).append(
                            '<tr role="row" class="abu-muda" data-id="' + n.id + '">' +
                            '<td style="text-align: center">' + n.no +
                            '</td><td style="text-align: center">' + n.hari +
                            '</td><td style="text-align: center">' + n.tgl +
                            '</td><td style="text-align: center">' + n.jam +
                            '</td><td style="text-align: center">' + n.absen_status +
                            //   '</td><td style="text-align: center">' + n.tgl_akhir +
                            '</td></tr>'
                            );


                });



            }


        }).fail(function () {
            $("#" + id).empty();
            $('#' + id).append("<tr><td colspan='6'>ERROR LOADING</td></tr>");
            //      alert('Error Loading');
        });
    }
    this.detailSiswaSaya = function (q) {
        $('#datadiri').empty();
        $('#datadiri').append("<img src='img/loading.gif' height='100px' /><br /><strong>Sedang memuat...</strong>");
        $('#dataortu').empty();
        $('#dataortu').append("<img src='img/loading.gif' height='100px' /><br /><strong>Sedang memuat...</strong>");

        $.ajax({
            type: "GET",
            url: "lib/detailSiswaSaya.jsp?q=" + q,
            dataType: "json",
        }).done(function (data) {
            $('#datadiri').empty();
            $('#dataortu').empty();
            if (data.length == 0) {
                $('#datadiri').append("DATA TIDAK DITEMUKAN");
                $('#dataortu').append("DATA TIDAK DITEMUKAN");
            } else {

                $.each(data, function (data, n) {
                    $('#datadiri').append(
                            '<br><ul><li style="float: left;display: block;width: 20%;">Tempat Lahir</li><li style="float: left;display: block;width: 2%;">:</li><li style="float: left;display: block;width: 70%;"><strong>' + n.tempat + '</strong></li></ul>'
                            + '<br><ul><li style="float: left;display: block;width: 20%;">Tanggal Lahir</li><li style="float: left;display: block;width: 2%;">:</li><li style="float: left;display: block;width: 70%;"><strong>' + n.tanggal + '</strong></li></ul>'
                            + '<br><ul><li style="float: left;display: block;width: 20%;">Umur</li><li style="float: left;display: block;width: 2%;">:</li><li style="float: left;display: block;width: 70%;"><strong>' + n.usia + '</strong></li></ul>'
                            + '<br><ul><li style="float: left;display: block;width: 20%;">Agama</li><li style="float: left;display: block;width: 2%;">:</li><li style="float: left;display: block;width: 70%;"><strong>' + n.agama + '</strong></li></ul>'
                            + '<br><ul><li style="float: left;display: block;width: 20%;">Jenis Kelamin</li><li style="float: left;display: block;width: 2%;">:</li><li style="float: left;display: block;width: 70%;"><strong>' + n.kelamin + '</strong></li></ul>'
                            + '<br><ul><li style="float: left;display: block;width: 20%;">Email</li><li style="float: left;display: block;width: 2%;">:</li><li style="float: left;display: block;width: 70%;"><a href="mailto:' + n.email + '?Subject=Email"><strong>' + n.email + '</strong></a></li></ul>'
                            + '<br><ul><li style="float: left;display: block;width: 20%;">No HP</li><li style="float: left;display: block;width: 2%;">:</li><li style="float: left;display: block;width: 70%;"><strong>' + n.no_hp + '</strong></li></ul>'

                            + '<br><ul><li style="float: left;display: block;width: 20%;">Tanggal Daftar</li><li style="float: left;display: block;width: 2%;">:</li><li style="float: left;display: block;width: 70%;"><strong>' + n.tanggal_daftar + '</strong></li></ul>'
                            + '<br><ul><li style="float: left;display: block;width: 20%;">Mengenal SMI</li><li style="float: left;display: block;width: 2%;">:</li><li style="float: left;display: block;width: 70%;"><strong>' + n.mengenal_smi + '</strong></li></ul>'

                            );
                    $('#dataortu').append(
                            '<br><ul><li style="float: left;display: block;width: 20%;">Nama Orang Tua</li><li style="float: left;display: block;width: 2%;">:</li><li style="float: left;display: block;width: 70%;"><strong>' + n.nama_ortu + '</strong></li></ul>'
                            + '<br><ul><li style="float: left;display: block;width: 20%;">No HP</li><li style="float: left;display: block;width: 2%;">:</li><li style="float: left;display: block;width: 70%;"><strong>' + n.nohp_ortu + '</strong></li></ul>'
                            + '<br><ul><li style="float: left;display: block;width: 20%;">No Tlp</li><li style="float: left;display: block;width: 2%;">:</li><li style="float: left;display: block;width: 70%;"><strong>' + n.notlp_ortu + '</strong></li></ul>'
                            + '<br><ul><li style="float: left;display: block;width: 20%;">Email</li><li style="float: left;display: block;width: 2%;">:</li><li style="float: left;display: block;width: 70%;"><strong>' + n.email_ortu + '</strong></li></ul>'

                            );

                });



            }


        }).fail(function () {
            $('#datadiri').empty();
            $('#dataortu').empty();
            $('#datadiri').append("ERROR LOADING... tekan F5 atau HUB ADMINISTRATOR");
            $('#dataortu').append("ERROR LOADING... tekan F5 atau HUB ADMINISTRATOR");
            //      alert('Error Loading');
        });
    }
    this.aa = function (variable) {
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == variable) {
                return pair[1];
            }
        }
        return(false);
    }


    function toRp(angka) {
        var rev = parseInt(angka, 10).toString().split('').reverse().join('');
        var rev2 = '';
        for (var i = 0; i < rev.length; i++) {
            rev2 += rev[i];
            if ((i + 1) % 3 === 0 && i !== (rev.length - 1)) {
                rev2 += '.';
            }
        }
        return rev2.split('').reverse().join('');
    }
    function toBulan(angka) {
        angka = angka.replace('-', '');
        var thn = angka.substring(0, 4);
        var bln = angka.substring(4, 6);
        var tgl = '';
        if (angka.length == 8) {
            tgl = angka.substring(6, 8) + ' ';
        }
        if (bln == '01') {
            bln = 'Januari';
        } else if (bln == '02') {
            bln = 'Februari';
        } else if (bln == '03') {
            bln = 'Maret';
        } else if (bln == '04') {
            bln = 'April';
        } else if (bln == '05') {
            bln = 'Mei';
        } else if (bln == '06') {
            bln = 'Juni';
        } else if (bln == '07') {
            bln = 'Juli';
        } else if (bln == '08') {
            bln = 'Agustus';
        } else if (bln == '09') {
            bln = 'September';
        } else if (bln == '10') {
            bln = 'Oktober';
        } else if (bln == '11') {
            bln = 'November';
        } else if (bln == '12') {
            bln = 'Desember';
        }

        return tgl + bln + ' ' + thn;
    }
    function toRpInt(angka) {
        var rev = angka.toString().split('').reverse().join('');
        var rev2 = '';
        for (var i = 0; i < rev.length; i++) {
            rev2 += rev[i];
            if ((i + 1) % 3 === 0 && i !== (rev.length - 1)) {
                rev2 += '.';
            }
        }
        return rev2.split('').reverse().join('');
    }
    this.showContent = function (id, url, judul) {
        $('#judulatas').empty();
        $('#judulatas').append(judul.replace("1", ""));
        $('#' + id).empty();
        $('#' + id).append('<iframe id="idframe" frameborder="0" src="' + url + '" width="100%" height="590" />');

    }
    return this;

})();



