

$(document).ready(function () {

    // sm.kolom();
    $("#m_jadwalsaya").click(function () {
        sm.jadwalsaya();
    });
    $("#m_datasaya").click(function () {
        sm.datasaya();
    });
    $("#m_pengajar").click(function () {
        sm.datapengajar();
    });
    $("#m_siswasaya").click(function () {
        sm.siswasaya();
    });
    $("#m_pendidikan").click(function () {
        sm.pendidikansaya();
    });
     $("#m_tagihan").click(function () {
        sm.tagihan();
    });
    $("#m_home").click(function () {
        sm.datahome();
    });
    $("#m_siswa").click(function () {
        sm.dashboardKesiswaan();
    });
    $("#m_dailyreport").click(function () {
        sm.dailyreport();
    });

    $("#m_finance").click(function () {
        sm.dashboardKeuangan();
    });
    $("#m_home_cso").click(function () {
        sm.datahomecso();
    });
    $("#m_jadwal").click(function () {
        sm.jadwalguru();
    });
    $("#m_budget").click(function () {
        sm.budget();
    });
     $("#m_evaluasi").click(function () {
        sm.evaluasi();
    });
    jQuery(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) { // on tab selection event
        jQuery(".demo-placeholder").each(function () { // target each element with the .contains-chart class
            var chart = jQuery(this).highcharts(); // target the chart itself
            chart.reflow() // reflow that chart
        });
    })

});
