<%@page import="org.smi.koneksiDatabase"%>
<%@page import="java.sql.Connection"%>

<%
    koneksiDatabase kb = new koneksiDatabase();
    Connection kon = kb.getKoneksiTujuan();
    String nama = request.getParameter("a");
    String order = request.getParameter("b");
    if (order == null) {
        order = "kr.nama";
    } else if (order.equalsIgnoreCase("jam")) {
        order = "FORMAT((SUM(v.jml * 15)/60), 1)";
    } else if (order.equalsIgnoreCase("siswa")) {
        order = "SUM(v.siswa)";
    }
    if (nama == null || nama.length() <= 2) {
        nama = "";
    } else {
        nama = " AND (nama LIKE '%" + nama + "%' OR panggilan LIKE '%" + nama + "%') ";
    }
    String sql = "SELECT ifnull(a.guid,'0') as id, UPPER(a.nama) as nama, UPPER(a.panggilan) as panggilan, LOWER(a.email) as email, a.member, a.hp, FORMAT(ifnull(jml, 0),0) as jml, id_tabungan, a.va, a.nama_referensi FROM pukat_pihak a "
            + "LEFT JOIN v_saldo_tabungan b ON b.id_pihak = a.id "
            + "WHERE isMember = 1" + nama + " "
            + "ORDER BY a.member";

    String js = kb.getDataJSONFromSQL(kon, sql);

    out.print("[" + js + "]");
    out.flush();
%>