<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.smi.koneksiDatabase"%>
<%
    Object user = session.getAttribute("judul");
    String html = "HALAMAN DEPAN";
    String nama = session.getAttribute("namasaya").toString();
    String userku = session.getAttribute("punyaku").toString();
    String menu = "";
    if (user != null) {
        html = user.toString();

    }
    try {
        koneksiDatabase kb = new koneksiDatabase();
        Connection kon = kb.getKoneksiTujuan();
        PreparedStatement stat = kon.prepareStatement("SELECT peran FROM musik_user_peran WHERE userid = '" + userku + "'");
        ResultSet rset = stat.executeQuery();
        while (rset.next()) {
            String peran = rset.getString(1);
            if (peran.contentEquals("PENGAJAR")) {
                menu = menu
                        + "<li><a href=\"#\" id=\"m_home\"><b>Halaman Depan</b></a></li>"
                        + "<li><a href=\"#\" id=\"m_jadwalsaya\"><b>Jadwal Saya</b></a></li>"
                        + "<li><a href=\"#\" id=\"m_siswasaya\"><b>Siswa Saya</b></a></li>"
                        + "<li><a href=\"#\" id=\"m_dailyreport\"><b>Daily Report</b></a></li>"
                        + "<li><a href=\"#\" id=\"m_evaluasi\"><b>Student Progres Report</b></a></li>"
                        //         + "<li><a href=\"#\" id=\"m_datasaya\"><b>Data Saya</b></a></li>"
                        + "<hr>";
            }
            if (peran.contentEquals("DASHBOARD")) {
                menu = menu
                        + "<li><a href=\"#\" id=\"m_siswa\"><b>Kesiswaan</b></a></li>"
                        + "<li><a href=\"#\" id=\"m_finance\"><b>Keuangan</b></a></li>"
                        + "<li><a href=\"#\" id=\"m_pengajar\"><b>Karyawan</b></a></li>"
                        + "<hr>"
                        + "<li><a href=\"#\" id=\"m_budget\"><b>Kontrol Pengeluaran</b></a></li>"
                        + "<hr>";

            } else if (peran.contentEquals("CSO")) {
                menu = menu
                        + "<li><a href=\"#\" id=\"m_home_cso\"><b>Halaman Depan</b></a></li>"
                        + "<li><a href=\"#\" id=\"m_jadwal\"><b>Jadwal Pengajar</b></a></li>"
                        + "<hr>";

            }
        }
    } catch (SQLException e) {
        menu = e.getMessage();
    }
%>
<style>
    .kepala{
        width:100%;
    
        height: 80px;
    }
</style>
<div class="kepala">
    <div class="col-lg-6 hidden-xs">
        <img src="img/logosmih.png" height="90px" />
    </div>
    <div class="col-lg-6 hidden-xs" style="margin-top: 6px;">
        <h2 style="color: blue; font-weight: bold; text-align: right;">SMART SCHOOL</h2>
    </div>
</div>
<div class="navbar" style="background-color: #13285c; min-height: 35px; color: white;">
    <div class="col-md-8">
        <div class="nav toggle" style="padding-top: 5px;">
            <a id="menu_toggle" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i></a>
            <ul class="dropdown-menu biru-skp">
                <%=menu%>
                <li><a href="#" id="m_pendidikan"><b>Kalender Pendidikan</b></a></li>
            </ul>
        </div>
        <span id="judulatas" style="font-size: 20px; font-weight: bold;"><%=html%></span>
    </div>
    <div class="col-md-4">
        <ul class="nav navbar-nav navbar-right">
            <strong><%=nama%></strong>
            <a href="#" id="keluar" style="margin-left: 20px;">
                <img src="img/ICON-KELUAR.png" width="30px;" style="margin-right: 10px;"></img>

            </a>
        </ul>
    </div>
</div>

<script type="text/javascript">

    $(function () {

        $('#keluar').click(function (e) {
            location.href = "keluar.jsp";
        });

    });
</script>