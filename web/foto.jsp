<%-- 
    Document   : foto
    Created on : Nov 29, 2016, 11:35:53 AM
    Author     : iweks
--%>

<%@page import="java.io.ByteArrayOutputStream"%>
<%@page import="java.awt.RenderingHints"%>
<%@page import="java.awt.AlphaComposite"%>
<%@page import="javax.imageio.ImageIO"%>
<%@page import="java.awt.Graphics2D"%>
<%@page import="java.awt.image.BufferedImage"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.sql.Blob"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.smi.koneksiDatabase"%>
<%@page import="java.sql.SQLException"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String kode = request.getParameter("kode");
    koneksiDatabase kb = new koneksiDatabase();
    Connection kon = kb.getKoneksiTujuan();
    try {
        String sql = "SELECT b.nilai FROM pukat_pihak a INNER JOIN pukat_pihak_file b ON b.id_daftar = a.id AND b.atribut = 'foto' "
                + "WHERE a.guid = '" + kode + "' ORDER BY b.id DESC LIMIT 1";

        PreparedStatement stat = kon.prepareStatement(sql);
        ResultSet rset = stat.executeQuery();
        if (rset.next()) {
            Blob blob = rset.getBlob(1);

        //    byte[] imgData = blob.getBytes(1, (int) blob.length());

            InputStream inputStream = blob.getBinaryStream();
            BufferedImage originalImage = ImageIO.read(inputStream);
            int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
            BufferedImage resizedImage = new BufferedImage(150, 100, type);
            Graphics2D g = resizedImage.createGraphics();
            g.drawImage(originalImage, 0, 0, 150, 100, null);
            g.dispose();
           
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(resizedImage, "png", baos);
            baos.flush();
            //  InputStream input = rset.getBinaryStream("foto");
            byte[] imageInByte = baos.toByteArray();
            response.setContentType("image/gif");
            OutputStream o = response.getOutputStream();
            o.write(imageInByte);
            o.flush();
            o.close();

            //    OutputStream output = response.getOutputStream();
        }
    } catch (SQLException sq) {

    } catch (Exception ex) {

    }
    kon.close();
%>