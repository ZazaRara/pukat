<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.smi.koneksiDatabase"%>
<%
    Object user = session.getAttribute("punyaku");
    Object hal = session.getAttribute("halaman");
    String html = "login.jsp";
    Object userk = session.getAttribute("judul");
    String judul = "HALAMAN DEPAN";
    String nama = "";//session.getAttribute("namasaya").toString();
    String userku = session.getAttribute("punyaku").toString();
    String menu = "";
    if (user == null) {
        response.sendRedirect("index.jsp");
    } else {
        try {
            koneksiDatabase kb = new koneksiDatabase();
            Connection kon = kb.getKoneksiTujuan();
            //      String peran = kb.getStringSQL(kon, "SELECT peran FROM musik_user_peran WHERE userid = '" + user + "'");
            String id_pihak = kb.getStringSQL(kon, "SELECT id_pihak FROM pukat_user WHERE userid = '" + user + "'");
            nama = kb.getStringSQL(kon, "SELECT nama FROM pukat_pihak WHERE id = '" + id_pihak + "'");

            session.setAttribute("idsaya", id_pihak);
            session.setAttribute("namasaya", nama);
            if (hal == null) {
                html = "home.jsp";

            } else {
                html = hal.toString();
            }
        } catch (Exception e) {
            out.print("SISTEM SEDANG SIBUK... MOHON DIULANGI BEBERAPA SAAT LAGI");
        }
    }

    if (userk != null) {
        judul = userk.toString();

    }
    try {
        koneksiDatabase kb = new koneksiDatabase();
        Connection kon = kb.getKoneksiTujuan();
        PreparedStatement stat = kon.prepareStatement("SELECT peran FROM pukat_user_peran WHERE userid = '" + userku + "'");
        ResultSet rset = stat.executeQuery();
        while (rset.next()) {
            String peran = rset.getString(1);
            if (peran.contentEquals("OPERATOR")) {
                menu = menu
                        + "<li><a href=\"#\" id=\"m_home\"><i class=\"fa fa-home fa-fw\"></i>&nbsp;&nbsp;<b>HALAMAN DEPAN</b></a></li>"
                        + "<li><a href=\"#\" id=\"m_siswa\"><i class=\"fa fa-users fa-fw\"></i>&nbsp;&nbsp;<b>DATA ANGGOTA</b></a></li>"
                        + "<li><a href=\"#\" id=\"m_tagihan\"><i class=\"fa fa-check-square-o fa-fw\"></i>&nbsp;&nbsp;<b>ANGGOTA BELUM APPROVE</b></a></li>"
                       + "<li><a href=\"#\" id=\"m_pengajar\"><i class=\"fa fa-ban fa-fw\"></i>&nbsp;&nbsp;<b>ANGGOTA TERBLOK</b></a></li>"
                        //        + "<li><a href=\"#\" id=\"m_budget\"><i class=\"fa fa-calendar-check-o fa-fw\"></i>&nbsp;&nbsp;<b>VERFIKASI ANGGOTA BARU</b></a></li>"
                        + "<hr>";
                //        + "<li><a href=\"#\" id=\"m_jadwalsaya\"><i class=\"fa fa-book fa-fw\"></i>&nbsp;&nbsp;<b>MASTER TABUNGAN</b></a></li>";

            } else if (peran.contentEquals("MEMBER")) {
                menu = menu
                        //      + "<li><a href=\"#\" id=\"m_home\"><i class=\"fa fa-home fa-fw\"></i>&nbsp;&nbsp;<b>HALAMAN DEPAN</b></a></li>"
                        //       + "<li><a href=\"#\" id=\"m_jadwalsaya\"><i class=\"fa fa-calendar fa-fw\" aria-hidden=\"true\"></i>&nbsp;&nbsp;<b>JADWAL SAYA</b></a></li>"
                        //       + "<li><a href=\"#\" id=\"m_siswasaya\"><i class=\"fa fa-users fa-fw\" aria-hidden=\"true\"></i>&nbsp;&nbsp;<b>SISWA SAYA</b></a></li>"
                        //       + "<li><a href=\"#\" id=\"m_dailyreport\"><i class=\"fa fa-dashboard fa-fw\" aria-hidden=\"true\"></i>&nbsp;&nbsp;<b>DAILY REPORT</b></a></li>"
                        //       + "<li><a href=\"#\" id=\"m_evaluasi\"><i class=\"fa fa-book fa-fw\" aria-hidden=\"true\"></i>&nbsp;&nbsp;<b>SPR</b></a></li>"
                        //         + "<li><a href=\"#\" id=\"m_datasaya\"><b>Data Saya</b></a></li>"
                        + "<hr>";
            } else if (peran.contentEquals("DASHBOARD")) {
                menu = menu
                        //        + "<li><a href=\"#\" id=\"m_home_cso\"><i class=\"fa fa-home fa-fw\"></i>&nbsp;&nbsp;<b>HALAMAN DEPAN</b></a></li>"
                        //         + "<li><a href=\"#\" id=\"m_pengajar\"><i class=\"fa fa-users fa-fw\"></i>&nbsp;&nbsp;<b>DATA PENGAJAR</b></a></li>"
                        //         + "<li><a href=\"#\" id=\"m_jadwal\"><i class=\"fa fa-calendar fa-fw\"></i>&nbsp;&nbsp;<b>JADWAL PENGAJAR</b></a></li>"
                        //         + "<li><a href=\"#\" id=\"m_tagihan\"><i class=\"fa fa-bank fa-fw\"></i>&nbsp;&nbsp;<b>TAGIHAN SPP</b></a></li>"
                        + "<hr>";

            }
        }
    } catch (SQLException e) {
        menu = e.getMessage();
    }
%>
<style>
    .kepala{
        width:100%;
        height: 10px;
    }
</style>

<div class="container">
    <div class="kepala hidden-xs">
        <div class="panel-6 hidden-xs">
            <img src="img/logopukat.jpeg" height="90px" class="hidden" />
        </div>
        <div class="panel-6 hidden-xs" style="margin-top: 6px;">
            <!-- <h2 style="color: #13285c; font-weight: bold; text-align: right; margin-right: 0;">SMART SCHOOL</h2> -->
        </div>
    </div>

    <div class="row">
        <div class="navbar bulat-kanan-kiri-atas" style="background-color: #303C5C; min-height: 30px; color: white;">
            <div class="col-md-8 col-xs-12">
                <div class="nav toggle" style="padding-top: 1px;">
                    <a id="menu_toggle" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bars"></i></a>
                    <ul class="dropdown-menu biru-skp" style="min-width: 250px; margin-left: 25px;">
                        <%=menu%>
                        <!--   <li><a href="#" id="m_pendidikan"><i class="fa fa-calendar fa-fw"></i>&nbsp;&nbsp;<b>KALENDER KEGIATAN</b></a></li> -->
                    </ul>
                </div>
                <span id="judulatas" class="hidden-xs" style="font-size: 20px; font-weight: bold;"><%=judul%></span>
                <span id="judulatas" class="visible-xs" style="font-size: 14px; font-weight: bold;"><%=judul%></span>
            </div>
            <div class="col-md-4 hidden-xs">
                <ul class="nav navbar-nav navbar-right hidden-xs">
                    <strong><%=nama%></strong>
                    <a href="#" id="keluar" style="margin-left: 20px;">
                        <img src="img/ICON-KELUAR.png" width="25px;" style="margin-right: 10px;"></img>
                    </a>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="panel-12" style="background-color: transparent; margin-top: -22px; min-height: 500px; border-style: solid; border-width: 1; border-color: #003973; border-top-color: transparent" id="dataUtama">
        </div>
    </div>
</div>
<script src="js/highcharts.js" type="text/javascript"></script>
<script src="js/highcharts-3d.js" type="text/javascript"></script>
<script src="js/exporting.js" type="text/javascript"></script>
<script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>
<script src="lib/home38.js" type="text/javascript"></script>
<script src="lib/loadAdmin32.js" type="text/javascript"></script>

<script type="text/javascript">

    var url = '<%=html%>';
    var id = 'dataUtama';
    $('#' + id).empty();
    $('#' + id).append("<div style='text-align:center;'><img src='img/loading.gif' height='200px' /><br /><strong>Sedang memuat...</strong></div>");
    $.get(url, function (data) {
        $('#' + id).empty();
        $('#' + id).html(data);
        //alert("Load was performed.");
    });
    $('#keluar').click(function (e) {
        location.href = "keluar.jsp";
    });

</script>



