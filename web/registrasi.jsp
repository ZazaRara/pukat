<%-- 
    Document   : daftar
    Created on : Apr 2, 2017, 9:20:20 PM
    Author     : iweks24
--%>



<%@page import="org.smi.komponenHTML"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="org.apache.commons.fileupload.FileItem"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.util.Map"%>
<%@page import="org.smi.koneksiDatabase"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    //Object user = session.getAttribute("punyaku");
    try {
        DiskFileItemFactory factory = new DiskFileItemFactory();

        ServletFileUpload sfu = new ServletFileUpload(factory);
        List items = sfu.parseRequest(request);
        Iterator awal = items.iterator();
        Iterator iter = items.iterator();
        koneksiDatabase kb = new koneksiDatabase();
        Connection kon = kb.getKoneksiSumber();
        String nama = "";
        String member = "";
        String iuran = "0";
        String tanggal_lahir = "1970-01-01";
        String tanggal_mulai = "1970-01-01";
        String tanggal_selesai = "1970-01-01";
        while (awal.hasNext()) {
            FileItem item = (FileItem) awal.next();
            if (item.isFormField()) {
                String a = item.getFieldName();
                String b = item.getString().toUpperCase().trim();
                b = b.replaceAll("\n", " - ");
                b = b.replaceAll("\r", "");
                b = b.replaceAll("\t", "");
                b = b.replaceAll("'", "''");
                b = b.replaceAll("\"", "''");
                if (a.equalsIgnoreCase("nama")) {
                    nama = b;
                } else if (a.equalsIgnoreCase("id_referensi")) {
                    member = b;
                } else if (a.equalsIgnoreCase("iuran")) {
                    iuran = b;
                } else if (a.equalsIgnoreCase("tanggal_lahir")) {
                    String tahun = b.substring(6, 10);
                    String bln = b.substring(3, 5);
                    String tg = b.substring(0, 2);
                    tanggal_lahir = tahun + "-" + bln + "-" + tg;
                } else if (a.equalsIgnoreCase("tanggal_mulai")) {
                    String tahun = b.substring(6, 10);
                    String bln = b.substring(3, 5);
                    String tg = b.substring(0, 2);
                    tanggal_mulai = tahun + "-" + bln + "-" + tg;
                } else if (a.equalsIgnoreCase("tanggal_selesai")) {
                    //   String tahun = b.substring(6, 10);
                    //    String bln = b.substring(3, 5);
                    //    String tg = b.substring(0, 2);
                    tanggal_selesai = b;
                }
            }
        }
        String[][] data = {
            {"nama", nama.toUpperCase()},
            {"parent_id", member},
            {"tanggal_lahir", tanggal_lahir},
            {"isMember", "2"},};
        int ok = kb.simpanDataReturnUUID(kon, "pukat_pihak", data, true);
        String kode = kb.getStringSQL(kon, "SELECT CONCAT(DATE_FORMAT(now(),'%y%m'), RIGHT(concat('0000',(SELECT count(1) FROM pukat_pihak WHERE DATE_FORMAT(from_date,'%Y%m') = DATE_FORMAT(now(),'%Y%m') AND id <= " + ok + " AND isMember IN(1,2,3))), 4)) as kode");
        String[][] data5 = {
            {"member", "06295" + kode},
            {"va", "06295" + kode},};
        kb.updateData(kon, "pukat_pihak", data5, "id = " + ok);
        if (ok <= 0) {
            response.sendRedirect("gagalSimpan.jsp");
        } else {
            String dataSelesai = kb.getStringSQL(kon, "SELECT tanggal_selesai FROM pukat_tabungan WHERE id = " + tanggal_selesai);
            String sqlTab = "INSERT INTO pukat_tabungan_pihak(id_pihak, id_tabungan,periode, masuk, saldo, guid, iuran) "
                    + "select " + ok + ", " + tanggal_selesai + ", DATE_FORMAT(a.Date, '%Y-%m') as tgl, 0 as masuk, 0 as saldo, UUID(), " + iuran + " from ("
                    + "select '" + dataSelesai + "' - INTERVAL (a.a + (10 * b.a) + (100 * c.a)) DAY as Date "
                    + "from (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as a "
                    + "cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as b "
                    + "cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as c) a "
                    + "where a.Date between '" + tanggal_mulai + "' and '" + dataSelesai + "' AND DAY(a.Date) = 1 ORDER BY a.Date";
            //   out.println(sqlTab);

            kb.setSQL(kon, sqlTab);

            while (iter.hasNext()) {
                FileItem item = (FileItem) iter.next();
                if (item.isFormField()) {
                    String a = item.getFieldName();
                    String b = item.getString();
                    String[][] data2 = {
                        {"atribut", a},
                        {"nilai", b.toUpperCase()},
                        {"id_daftar", String.valueOf(ok)},};
                    kb.simpanData(kon, "pukat_pihak_detail", data2, true);
                    String[][] data3 = {
                        {a, b.toUpperCase()},};
                    kb.updateData(kon, "pukat_pihak", data3, "id = " + ok);
                } else {
                    String fieldName = item.getFieldName();
                    byte[] b = item.get();
                    kb.simpanDataBlob(kon, "INSERT INTO pukat_pihak_file(id_daftar, atribut, nilai) VALUES(" + ok + ",'" + fieldName + "',?)", b);
                }
            }
            kon.close();
            response.sendRedirect("suksesSimpan.jsp");
        }
    } catch (Exception e) {

        koneksiDatabase kb = new koneksiDatabase();
        Connection kon = kb.getKoneksiSumber();
        komponenHTML html = new komponenHTML();

        String hasil = html.option(kon, "SELECT id, nama_program FROM pukat_tabungan WHERE thru_date IS NULL");
        String pekerjaan = html.option(kon, "SELECT nama, nama FROM pukat_master_list WHERE tipe = 'PEKERJAAN' ORDER BY urut");
        //    String paroki = html.option(kon, "SELECT nama, nama FROM pukat_master_list WHERE tipe = 'PAROKI' ORDER BY urut");
        String jabatan = html.option(kon, "SELECT nama, nama FROM pukat_master_list WHERE tipe = 'JABATAN' ORDER BY urut");
        String bidang = html.option(kon, "SELECT nama, nama FROM pukat_master_list WHERE tipe = 'BIDANG_INDUSTRI' ORDER BY nama");
        //       String paro = html.option(kon, "SELECT nama, nama FROM mst WHERE tipe = 'JABATAN' ORDER BY urut");

        //   String propinsi = html.option(kon, "SELECT kode_wilayah, nama FROM mst_wilayah WHERE id_level_wilayah = '1' AND kode_wilayah != '350000' AND expired_date IS NULL ORDER BY nama");
        kon.close();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PENDAFTARAN ANGGOTA</title>
        <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css'>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/skp_home.css" rel="stylesheet" type="text/css"/>
        <link href="css/daftar.css" rel="stylesheet" type="text/css"/>
        <link href="css/checkbox.css" rel="stylesheet" type="text/css"/>
        <style>
            #progressbar li {
                list-style-type: none;
                color: white;
                text-transform: uppercase;
                font-size: 15px;
                width: 12%;
                float: left;
                position: relative;
            }
        </style>
    </head>
    <body style="background-color: #303C5C;">
        <!-- multistep form -->
        <form id="msform" action="#" method="POST" enctype="multipart/form-data">
            <!-- progressbar -->
            <ul id="progressbar">
                <li class="active">Pribadi</li>
                <li>Kontak</li>
                <li>Alamat</li>
                <li>Pekerjaan</li>
                <li>Referensi</li>
                <li>Tabungan</li>
                <li>Minat</li>
                <li>Dokumen</li>
            </ul>
            <!-- fieldsets -->
            <fieldset>
                <h2 class="fs-title">Data Pribadi Calon Anggota</h2>
                <h3 class="fs-subtitle">*) data harus diisi</h3>

                <div class="panel-12" style="text-align: left;"><label>Nama Lengkap *)</label></div>
                <input type="text" name="nama" style="text-transform:uppercase" required />
                <div class="panel-12" style="text-align: left;"><label>Nama Baptis *)</label></div>
                <input type="text" name="nama_baptis" style="text-transform:uppercase" required />
                <div class="panel-12" style="text-align: left;"><label>Nama Panggilan</label></div>
                <input type="text" name="panggilan" style="text-transform:uppercase" />
                <div class="panel-12">
                    <div class="panel-8">
                        <div class="panel-12" style="text-align: left;"><label>Tempat Lahir</label></div>
                        <input type="text" name="tempat_lahir" style="text-transform:uppercase" />
                    </div>
                    <div class="panel-1">
                        &nbsp;
                    </div>

                    <div class="panel-3">
                        <div class="panel-12" style="text-align: left;"><label>Tanggal Lahir </label></div>
                        <input type="text" name="tanggal_lahir" placeholder="DD/MM/YYYY" id="datepicker" />
                    </div>
                </div>
                <div class="panel-12" style="text-align: left;"><label>No KTP *)</label></div>
                <input type="text" name="ktp" required />
                <div class="panel-12" style="text-align: left;"><label>Hobby / Kegemaran </label></div>
                <input type="text" name="hobi" style="text-transform:uppercase" />
                <input type="button" name="next" class="next action-button" value="Selanjutnya >>" style="float: right;" />
            </fieldset>

            <fieldset>
                <h2 class="fs-title">Kontak Person Calon Anggota</h2>
                <h3 class="fs-subtitle">*) data harus diisi</h3>
                <div class="panel-12" style="text-align: left;"><label>Nomor Handphone *)</label></div>
                <input type="text" name="hp" id="hp" required />

                <div class="panel-12" style="text-align: left;"><label>Nomor WhatsApp (WA)</label></div>
                <div class="panel-1" style="text-align: left;">
                    <input type="checkbox" name="cek_sama_hp" value="cek_sama_hp" onclick='handleClick(this);'>
                </div>
                <div class="panel-11" style="text-align: left;">Sama dengan No HP</div>
                <input type="text" name="wa" id="wa"  />
                <div class="panel-12">
                    <div class="panel-1" style="text-align: left;">
                        <input type="checkbox" name="cek_wa" value="cek_wa">
                    </div>
                    <div class="panel-11" style="text-align: left;">Ditambahkan pada Group WA YPS</div>

                </div>
                <div class="panel-12" style="text-align: left;"><label>Email Pribadi *)</label></div>
                <input type="text" name="email" required />
                <div class="panel-12" style="text-align: left;"><label>PIN BBM</label></div>
                <input type="text" name="bbm" />
                <div class="panel-12" style="text-align: left;"><label>Facebook</label></div>
                <input type="text" name="facebook"  />
                <input type="button" name="previous" class="previous action-button" value="<< Kembali" style="float: left; background-color: #ec971f" />
                <input type="button" name="next" class="next action-button" value="Selanjutnya >>" style="float: right;" />
            </fieldset>

            <fieldset>
                <h2 class="fs-title">Alamat Calon Anggota</h2>
                <h3 class="fs-subtitle">*) data harus diisi sesuai KTP</h3>

                <div class="panel-12" style="text-align: left;"><label>Alamat Rumah</label></div>
                <textarea name="alamat1" style="text-transform:uppercase"></textarea>
                <div class="panel-12" style="text-align: left;"><label>RT/RW Kelurahan</label></div>
                <input type="text" style="text-transform:uppercase" name="alamat2" />

                <div class="panel-3" style="text-align: left;">
                    <div class="panel-12" style="text-align: left;"><label>Propinsi</label></div>
                    <select id="propinsi" name="propinsi" class="form-control" style="margin-bottom: 15px;">

                    </select>
                    <!-- <input type="text" style="text-transform:uppercase" name="propinsi" required /> -->
                </div>
                <div class="panel-1" style="text-align: left;">
                    &nbsp;
                </div>
                <div class="panel-3" style="text-align: left;">
                    <div class="panel-12" style="text-align: left;"><label>Kabupaten/Kota</label></div>
                    <select id="kabupaten" name="kabupaten" class="form-control" style="margin-bottom: 15px;" >

                    </select>
                </div>
                <div class="panel-1" style="text-align: left;">
                    &nbsp;
                </div>
                <div class="panel-4" style="text-align: left;">
                    <div class="panel-12" style="text-align: left;"><label>Kecamatan</label></div>
                    <select id="kecamatan" name="kecamatan" class="form-control" style="margin-bottom: 15px;">

                    </select>
                </div>

                <hr>
                <div class="panel-1" style="text-align: left;">
                    <input type="checkbox" name="cek_alamat" value="cek_alamat" onclick='alamatSurat(this);'>
                </div>
                <div class="panel-11" style="text-align: left;">Alamat Surat sama dengan Alamat Rumah</div>
                <div id="surat" class="panel-12">
                    <div class="panel-12" style="text-align: left;"><label>Alamat Surat-Menyurat)</label></div>
                    <textarea name="alamat1_surat" style="text-transform:uppercase"></textarea>
                    <div class="panel-12" style="text-align: left;"><label>RT/RW Kelurahan</label></div>
                    <input type="text" style="text-transform:uppercase" name="alamat2_surat" />
                    <div class="panel-12">
                        <div class="panel-3" style="text-align: left;">
                            <div class="panel-12" style="text-align: left;"><label>Propinsi</label></div>
                            <select id="propinsi_surat" name="propinsi_surat" class="form-control" style="margin-bottom: 15px;">

                            </select>
                            <!-- <input type="text" style="text-transform:uppercase" name="propinsi" required /> -->
                        </div>
                        <div class="panel-1" style="text-align: left;">
                            &nbsp;
                        </div>
                        <div class="panel-3" style="text-align: left;">
                            <div class="panel-12" style="text-align: left;"><label>Kabupaten/Kota</label></div>
                            <select id="kabupaten_surat" name="kabupaten_surat" class="form-control" style="margin-bottom: 15px;">

                            </select>
                        </div>
                        <div class="panel-1" style="text-align: left;">
                            &nbsp;
                        </div>
                        <div class="panel-3" style="text-align: left;">
                            <div class="panel-12" style="text-align: left;"><label>Kecamatan</label></div>
                            <select id="kecamatan_surat" name="kecamatan_surat" class="form-control" style="margin-bottom: 15px;">

                            </select>
                        </div>

                    </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="<< Kembali" style="float: left; background-color: #ec971f" />
                <input type="button" name="next" class="next action-button" value="Selanjutnya >>" style="float: right;" />
            </fieldset>


            <fieldset>
                <h2 class="fs-title">Pekerjaan Calon Anggota</h2>
                <h3 class="fs-subtitle">*) data harus diisi</h3>
                <div class="panel-12" style="text-align: left;"><label>Pekerjaan</label></div>
                <select id="pekerjaan" name="pekerjaan" class="form-control" style="margin-bottom: 15px;">
                    <%=pekerjaan%>
                </select>
                <div class="panel-12" style="text-align: left;"><label>Bidang Insdustri</label></div>
                <select id="bidang" name="bidang" class="form-control" style="margin-bottom: 15px;">
                    <%=bidang%>
                </select>
                <!-- <input type="text" name="pekerjaan" style="text-transform:uppercase" required /> -->
                <div class="panel-12" style="text-align: left;"><label>Nama Instansi/Sekolah</label></div>
                <input type="text" name="bidang" style="text-transform:uppercase" />
                <div class="panel-12" style="text-align: left;"><label>Jabatan dalam Perusahaan</label></div>
                <select id="jabatan" name="jabatan" class="form-control" style="margin-bottom: 15px;">
                    <%=jabatan%>
                </select>
                <!--    <input type="text" name="jabatan" style="text-transform:uppercase" required /> -->
                <input type="button" name="previous" class="previous action-button" value="<< Kembali" style="float: left; background-color: #ec971f" />
                <input type="button" name="next" class="next action-button" value="Selanjutnya >>" style="float: right;" />
            </fieldset>

            <fieldset>
                <h2 class="fs-title">Rerefensi Keanggotaan</h2>
                <h3 class="fs-subtitle">*) data harus diisi</h3>
                <div class="panel-12" style="text-align: left;"><label>Nama Paroki *)</label></div>
                <div class="panel-4" style="text-align: left;">
                    <select id="wilParoki" name="wilParoki" class="form-control" style="margin-bottom: 15px;" required="">

                    </select>
                </div>
                <div class="panel-1" style="text-align: left;">
                    &nbsp;
                </div>
                <div class="panel-7" style="text-align: left;">
                    <select id="paroki" name="paroki" class="form-control" style="margin-bottom: 15px;" required="">

                    </select>
                </div>
                <!-- <input type="text" name="paroki" style="text-transform:uppercase"  required /> -->
                <div class="panel-12" style="text-align: left;"><label>ID Anggota Pemberi Referensi</label></div>
                <input type="text" name="id_referensi" />
                <div class="panel-12" style="text-align: left;"><label>Nama Pemberi Referensi</label></div>
                <input type="text" name="nama_referensi" style="text-transform:uppercase" />
                <div class="panel-12" style="text-align: left;"><label>No. HP Pemberi Referensi</label></div>
                <input type="text" name="hp_referensi" />
                <input type="button" name="previous" class="previous action-button" value="<< Kembali" style="float: left; background-color: #ec971f" />
                <input type="button" name="next" class="next action-button" value="Selanjutnya >>" style="float: right;" />
            </fieldset>
            <fieldset>
                <h2 class="fs-title">Pundi Berkat Pukat</h2>
                <h3 class="fs-subtitle">*) data harus diisi</h3>
                <div class="panel-12" style="text-align: left;"><label>Nama Tabungan *)</label></div>
                <select id="tanggal_selesai" name="tanggal_selesai" class="form-control" style="margin-bottom: 15px;" required="">
                    <%=hasil%>
                </select>
                <div class="panel-12" style="text-align: left;"><label>Tanggal Mulai Iuran Tabungan *)</label></div>
                <input type="text" name="tanggal_mulai" placeholder="DD/MM/YYYY" id="tanggal_mulai" required />
                <div class="panel-12" style="text-align: left;"><label>Besaran Iuran Tabungan *)</label></div>
                <select id="iuran" name="iuran" class="form-control" style="margin-bottom: 15px;" required="">
                    <option value="250000">&nbsp;&nbsp;Rp. 250.000 / bulan&nbsp;&nbsp;</option>
                    <option value="500000" selected>&nbsp;&nbsp;Rp. 500.000 / bulan&nbsp;&nbsp;</option>
                    <option value="1000000">&nbsp;&nbsp;Rp. 1.000.000 / bulan&nbsp;&nbsp;</option>

                </select>
                <div class="panel-12" style="text-align: left;"><label>Metode Transfer *)</label></div>
                <select id="tbb" name="tbb" class="form-control" style="margin-bottom: 15px;" required="">
                    <option value="transfer" selected>&nbsp;&nbsp;Transfer ke Rekening BCA YPS</option>
                    <option value="debet" selected>&nbsp;&nbsp;Auto Debet dari Rekening BCA Anda&nbsp;&nbsp;</option>
                </select>
                <input type="button" name="previous" class="previous action-button" value="<< Kembali" style="float: left; background-color: #ec971f" />
                <input type="button" name="next" class="next action-button" value="Selanjutnya >>" style="float: right;" />

            </fieldset>
            <fieldset>
                <h2 class="fs-title">Info Anggota</h2>
                <h3 class="fs-subtitle">*) data diisi sesuai keinginan</h3>


                <div class="panel-12" style="text-align: left;"><label>Interest/Minat</label></div>
                <div class="panel-12">
                    <div class="panel-4">
                        <div class="funkyradio">
                            <div class="funkyradio-success">
                                <input type="checkbox" name="minat01" id="m1"/>
                                <label for="m1">Property</label>
                            </div>  
                            <div class="funkyradio-success">
                                <input type="checkbox" name="minat02" id="m2"/>
                                <label for="m2">Perbankan</label>
                            </div> 
                            <div class="funkyradio-success">
                                <input type="checkbox" name="minat03" id="m3"/>
                                <label for="m3">Keuangan</label>
                            </div> 
                            <div class="funkyradio-success">
                                <input type="checkbox" name="minat04" id="m4"/>
                                <label for="m4">Konsultan</label>
                            </div> 
                        </div>
                    </div>
                    <div class="panel-4">
                        <div class="funkyradio">
                            <div class="funkyradio-success">
                                <input type="checkbox" name="minat05" id="m5"/>
                                <label for="m5">Pendidikan/Training</label>
                            </div>
                            <div class="funkyradio-success">
                                <input type="checkbox" name="minat06" id="m6"/>
                                <label for="m6">Konstruksi</label>
                            </div> 
                            <div class="funkyradio-success">
                                <input type="checkbox" name="minat07" id="m7"/>
                                <label for="m7">Kuliner/makanan</label>
                            </div> 
                            <div class="funkyradio-success">
                                <input type="checkbox" name="minat08" id="m8"/>
                                <label for="m8">Kontraktor</label>
                            </div>                         
                        </div>
                    </div>
                    <div class="panel-4">
                        <div class="funkyradio">
                            <div class="funkyradio-success">
                                <input type="checkbox" name="minat09" id="m9"/>
                                <label for="m9">Otomotif</label>
                            </div>
                            <div class="funkyradio-success">
                                <input type="checkbox" name="minat10" id="m10"/>
                                <label for="m10">Transportasi</label>
                            </div> 
                            <div class="funkyradio-success">
                                <input type="checkbox" name="minat11" id="m11"/>
                                <label for="m11">Electricity</label>
                            </div> 
                            <div class="funkyradio-success">
                                <input type="checkbox" name="minat12" id="m12"/>
                                <label for="m12">Elektronics</label>
                            </div>                         
                        </div>
                    </div>
                </div>
                <div class="panel-12" style="text-align: left;"><label>Motivasi mengikuti Pundi Berkat Pukats</label></div>
                <div class="panel-12">
                    <div class="panel-6">
                        <div class="funkyradio">
                            <div class="funkyradio-success">
                                <input type="checkbox" name="motivasi01" id="s1"/>
                                <label for="s1">Menambah Wawasan</label>
                            </div>  
                            <div class="funkyradio-success">
                                <input type="checkbox" name="motivasi02" id="s2"/>
                                <label for="s2">Sebagai Anggota Biasa</label>
                            </div> 
                            <div class="funkyradio-success">
                                <input type="checkbox" name="motivasi03" id="s3"/>
                                <label for="s3">Menyalurkan Berkat</label>
                            </div> 
                            <div class="funkyradio-success">
                                <input type="checkbox" name="motivasi04" id="s4"/>
                                <label for="s4">Memberikan Pendampingan</label>
                            </div> 
                            <div class="funkyradio-success">
                                <input type="checkbox" name="motivasi05" id="s5"/>
                                <label for="s5">Memberikan training</label>
                            </div> 
                        </div>
                    </div>
                    <div class="panel-6">
                        <div class="funkyradio">
                            <div class="funkyradio-success">
                                <input type="checkbox" name="motivasi06" id="s6"/>
                                <label for="s6">Memiliki Komunitas & Kegiatan Positif</label>
                            </div>
                            <div class="funkyradio-success">
                                <input type="checkbox" name="motivasi07" id="s7"/>
                                <label for="s7">Ikut berperan dalam kegiatan Pukat</label>
                            </div> 
                            <div class="funkyradio-success">
                                <input type="checkbox" name="motivasi08" id="s8"/>
                                <label for="s8">Memberikan Pelayanan</label>
                            </div> 
                            <div class="funkyradio-success">
                                <input type="checkbox" name="motivasi09" id="s9"/>
                                <label for="s9">Sharing Pengalaman</label>
                            </div>                         
                        </div>
                    </div>
                </div>
                <input type="button" name="previous" class="previous action-button" value="<< Kembali" style="float: left; background-color: #ec971f" />
                <input type="button" name="next" class="next action-button" value="Selanjutnya >>" style="float: right;" />
            </fieldset>
            <fieldset>
                <h2 class="fs-title">Upload Dokumen</h2>
                <h3 class="fs-subtitle"></h3>
                Upload File Foto: <input type="file" name="foto"/><br />
                Upload File KTP: <input type="file" name="file_ktp"/><br />
                <input type="button" name="previous" class="previous action-button" value="<< Kembali" style="float: left; background-color: #ec971f" />
                <input type="submit" name="submit" class="submit action-button" value="Submit" style="float: right; background-color: blue;" />
            </fieldset>
        </form>
        <script src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
        <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src='//cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.min.js'></script>
        <script src='//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
        <script src="js/daftarMenu.js" type="text/javascript"></script>

        <script>
                        $(function () {

                            $("#datepicker").datepicker({
                                yearRange: '1910:2010',
                                changeMonth: true,
                                changeYear: true,
                                dateFormat: 'dd/mm/yy',
                                defaultDate: '01/01/1970'
                            });
                            $("#tanggal_mulai").datepicker({
                                //   yearRange: '1910:2010',
                                //    changeMonth: true,
                                //    changeYear: true,
                                dateFormat: 'dd/mm/yy',
                                //    defaultDate: '01/01/1970'
                            });
                            $('#msform').one('submit', function () {
                                $(this).find('input[type="submit"]').attr('disabled', 'disabled');
                            });
                            wilayah('propinsi', '000000');
                            paroki('wilParoki', '0');
                            paroki('paroki', '1');
                            $('#propinsi').change(function (e) {
                                var a = $('#propinsi').val();
                                wilayah('kabupaten', a);
                                var a = $('#kabupaten').val();
                                wilayah('kecamatan', a);
                            });

                            $('#kabupaten').change(function (e) {
                                var a = $('#kabupaten').val();
                                wilayah('kecamatan', a);
                            });
                            $('#wilParoki').change(function (e) {
                                var a = $('#wilParoki').val();
                                paroki('paroki', a);
                            });
                            wilayah('propinsi_surat', '000000');

                            $('#propinsi_surat').change(function (e) {
                                var a = $('#propinsi_surat').val();
                                wilayah('kabupaten_surat', a);
                                var a = $('#kabupaten_surat').val();
                                wilayah('kecamatan_surat', a);
                            });

                            $('#kabupaten_surat').change(function (e) {
                                var a = $('#kabupaten_surat').val();
                                wilayah('kecamatan_surat', a);
                            });

                            function wilayah(id, kode) {

                                $("#" + id).empty();
                                $.ajax({
                                    type: "GET",
                                    url: "lib/listWilayah.jsp?q=" + kode,
                                    dataType: "json",
                                }).done(function (data) {
                                    $("#" + id).empty();

                                    $.each(data, function (data, n) {
                                        $("#" + id).append(
                                                '<option value="' + n.kode_wilayah + '">&nbsp;&nbsp;' + n.nama + '&nbsp;&nbsp;</option>'

                                                );
                                    });

                                }).fail(function () {
                                    $("#" + id).empty();
                                    $('#' + id).append("<tr><td colspan='6'>ERROR LOADING</td></tr>");
                                    //      alert('Error Loading');
                                });
                            }
                            function paroki(id, kode) {

                                $("#" + id).empty();
                                $.ajax({
                                    type: "GET",
                                    url: "lib/listParoki.jsp?q=" + kode,
                                    dataType: "json",
                                }).done(function (data) {
                                    $("#" + id).empty();

                                    $.each(data, function (data, n) {
                                        if (kode == '0') {
                                            $("#" + id).append(
                                                    '<option value="' + n.id + '">&nbsp;&nbsp;' + n.nama + '&nbsp;&nbsp;</option>'

                                                    );
                                        } else {
                                            $("#" + id).append(
                                                    '<option value="' + n.nama + '">&nbsp;&nbsp;' + n.nama + '&nbsp;&nbsp;</option>'

                                                    );
                                        }
                                    });

                                }).fail(function () {
                                    $("#" + id).empty();
                                    $('#' + id).append("<tr><td colspan='6'>ERROR LOADING</td></tr>");
                                    //      alert('Error Loading');
                                });
                            }
                        });

                        function handleClick(cb) {
                            if (cb.checked) {
                                var a = $('#hp').val();
                                $('#wa').val(a);
                            } else {
                                $('#wa').val('');
                            }
                        }

                        function alamatSurat(cb) {
                            if (cb.checked) {
                                $("#surat").hide();
                            } else {
                                $("#surat").show();
                            }
                        }
        </script>
    </body>
</html>
<%    }
%>
