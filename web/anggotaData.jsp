
<%@page import="java.text.DecimalFormat"%>
<%@page import="java.sql.Connection"%>
<%@page import="org.smi.koneksiDatabase"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    session.setAttribute("halaman", "anggotaData.jsp");
    session.setAttribute("judul", "DATA ANGGOTA");
%>
<div class="panel-12" style="margin-top: 20px;">
    <div class="col-lg-12 col-xs-12" id="atas-guru" style="margin-bottom: 15px; color: blue;">
        <div class="form-inline">
            <div class="panel-9">
                Filter : <input type="text" id="filter" style="width: 250px;" class="form-control">
                &nbsp;&nbsp;&nbsp;&nbsp;
                View :
                <select id="view" class="form-control">
                    <option value="kanban" selected="">&nbsp;&nbsp;CARD&nbsp;&nbsp;</option>
                    <option value="tabel">&nbsp;&nbsp;TABEL&nbsp;&nbsp;</option>
                </select>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="#" style="margin-top: -10px;" class="btn btn-default" id="show">Tampil Data</a>
            </div>
            <div class="panel-3" style="text-align: right; margin-top: -10px;">
                <a href="#" class="btn btn-success" id="add"><i class="fa fa-user-plus fa-fw"></i>&nbsp;&nbsp;Tambah Anggota</a>
            </div>
        </div>
    </div>
    <hr>
    <div class="col-lg-12 col-xs-12" id="rekap-guru">

    </div>
</div>
<div class="modal fade" id="detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top: -100px;">
    <div class="modal-dialog modal-lg">
        <div style="background-color: white; text-align: right;">&nbsp;
            <a class="btn btn-danger" data-dismiss="modal" style="margin-top: -5; color: red;"><strong>Tutup</strong></a>         
        </div>
        <iframe width="100%" height="90%" frameborder="0" allowtransparency="true" id="frame"></iframe>  
    </div>
</div>
<div class="modal fade" id="detail2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top: -100px;">
    <div class="modal-dialog modal-lg">
        <div style="background-color: white; text-align: right;">&nbsp;
            <a class="btn btn-danger" data-dismiss="modal" style="margin-top: -5; color: red;"><strong>Tutup</strong></a>         
        </div>
        <iframe width="100%" height="90%" frameborder="0" allowtransparency="true" id="frame2"></iframe>  
    </div>
</div>
<div class="modal fade" id="transfer" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="margin-top: -50px;">
    <div class="modal-dialog modal-lg">
        <div class="modal-header-skp">INPUT TRANSFER UANG
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <br />
        </div>
        <iframe width="100%" height="75%" frameborder="0" allowtransparency="true" id="transferurl"></iframe>  
    </div>
</div>
<script type="text/javascript">

    $(function () {
        $("#frame").attr('src', 'loading.jsp');
        $("#frame2").attr('src', 'loading.jsp');
        proses();
        $('#view').change(function (e) {
            proses();
        });
        $('#show').click(function (e) {
            proses();
        });
        $('#add').click(function (e) {
            $("#frame").attr('src', 'daftar.jsp?q=');
            $("#detail").modal();
        });
        $('#detail').on('hidden.bs.modal', function () {
            $("#frame").attr('src', 'loading.jsp');
            proses();
        });
        $('#detail2').on('hidden.bs.modal', function () {
            $("#frame2").attr('src', 'loading.jsp');
            proses();
        });
        $('#transfer').on('hidden.bs.modal', function () {
            $("#transferurl").attr('src', 'loading.jsp');
            proses();
        });
        function proses() {
            var id = 'rekap-guru';
            var a = $("#filter").val();
            var b = $("#order").val();
            var view = $("#view").val();
            $("#" + id).empty();
            $('#' + id).append("<img src='img/loading.gif' height='100px' /><br /><strong>Sedang memuat...</strong>");
            $.ajax({
                type: "GET",
                url: "lib/listDataAnggota.jsp?a=" + a + "&b=" + b,
                dataType: "json",
            }).done(function (data) {
                $("#" + id).empty();
                if (data.length == 0) {
                    $('#' + id).append("DATA TIDAK DITEMUKAN");
                } else {
                    if (view == 'tabel') {
                        $('#' + id).append('<div class="card-content table-responsive">' +
                                '<table class="table table-hover">' +
                                '<thead class="text-warning">' +
                                '<tr>' +
                                '<th style="width: 10px;">No</th>' +
                                '<th style="width: 150px;text-align: center">Nama Anggota</th>' +
                                '<th style="width: 100px;text-align: center">Panggilan</th>' +
                                '<th style="width: 100px;text-align: center">No Anggota</th>' +
                                '<th style="width: 100px;text-align: center">Email</th>' +
                                '<th style="width: 100px;text-align: center">No HP</th>' +
                                '<th style="width: 100px;text-align: center">Referensi</th>' +
                                '<th style="width: 120px;text-align: center">Saldo Akhir</th>' +
                                //    '<th style="width: 120px;text-align: center">Jml Report *)</th>' +
                                '</tr>' +
                                '</thead>' +
                                '<tbody id="tTagih">' +
                                '</tbody>' +
                                '</table>' +
                                '</div>');
                    }
                    $.each(data, function (data, n) {

                        var add = '<a href=# id="tab' + n.id + '" class="btn btn-primary" style="margin-bottom:5px;"><i class="fa fa-money fa-fw"></i>&nbsp;&nbsp;Transfer Uang</a>';
                        if (view == 'tabel') {
                            $('#tTagih').append(
                                    '<tr role="row" class="abu-muda">' +
                                    '<td>' + n.no +
                                    '</td><td style="text-align: left"><a href="#" id="rekap' + n.id + '">' + n.nama + '</a>' +
                                    '</td><td style="text-align: center">' + n.panggilan +
                                    '</td><td style="text-align: center">' + n.member +
                                    '</td><td style="text-align: center">' + n.email +
                                    '</td><td style="text-align: center">' + n.hp +
                                    '</td><td style="text-align: left">' + n.nama_referensi +
                                    '</td><td style="text-align: center"><a href="#" id="transf' + n.id + '">' + n.jml +'</a>' +
                                    "</td></tr>"
                                    );
                        } else {
                            var xx = '<div class="circle-tile col-xs-12 col-lg-3" style="padding-left:5px;" >'
                                    + '<div class="circle-tile-footer"><div class="text-hitam-tebal tebal weks"><a href="#" id="rekap2' + n.id + '"><u>' + n.nama
                                    + '</u><br /><i class="jabatan-staff">' + n.member
                                    + '</i></a></div></div><div class="biru-skp"><a href="#" id="rekap' + n.id + '"><img src="foto.jsp?kode=' + n.id
                                    + '" alt="" class="gambar-staff-tengah" onerror="this.src=\'img/foto.gif\'"></a><div class="text-biru-tua-tebal">Rp. ' + n.jml.replace(/,/g, ".")
                                    + '</div></div>'
                                    + '<div class="text-hitam-tebal tebal weks biru-skp">'
                                    + '<i class=\"fa fa-home fa-fw\"></i> VA BCA :&nbsp;&nbsp;' + n.va
                                    + '<br><i class=\"fa fa-mobile fa-fw\"></i>&nbsp;&nbsp;' + n.hp + '</div>'
                                    + '<div class="circle-tile-footer text-hitam-tebal">' + add + '</div></div>';
                            $('#' + id).append(
                                    xx
                                    );
                        }


                        $('#tab' + n.id).click(function (e) {
                            $("#transferurl").attr('src', 'anggotaTransfer.jsp?a=' + n.id + '&b=' + n.id_tabungan + '&c=' + n.nama + '&d=' + n.member + '&e=' + n.jml);
                            $("#transfer").modal();
                        });
                         $('#transf' + n.id).click(function (e) {
                            $("#transferurl").attr('src', 'anggotaTransfer.jsp?a=' + n.id + '&b=' + n.id_tabungan + '&c=' + n.nama + '&d=' + n.member + '&e=' + n.jml);
                            $("#transfer").modal();
                        });
                        $('#rekap' + n.id).click(function (e) {
                            $("#frame2").attr('src', 'dataAnggota.jsp?q=' + n.id);
                            $("#detail2").modal();
                            //  alert('SEDANG DALAM PEMBUATAN');
                        });
                        $('#rekap2' + n.id).click(function (e) {
                            $("#frame2").attr('src', 'dataAnggota.jsp?q=' + n.id);
                            $("#detail2").modal();
                            //  alert('SEDANG DALAM PEMBUATAN');
                        });

                    });



                }


            }).fail(function () {
                $("#" + id).empty();
                $('#' + id).append("<tr><td colspan='6'>ERROR LOADING</td></tr>");
                //      alert('Error Loading');
            });
        }

    });
</script>
