<%-- 
    Document   : inputTransaksi
    Created on : Nov 23, 2016, 2:45:32 PM
    Author     : iweks
--%>


<%@page import="java.sql.Connection"%>
<%@page import="org.smi.koneksiDatabase"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    String userid = request.getParameter("id_pihak");
    String jenis = request.getParameter("id_tabungan");
    koneksiDatabase kb = new koneksiDatabase();
    Connection kon = kb.getKoneksiTujuan();

    String userk = session.getAttribute("punyaku").toString();
    int hak = kb.getIntSQL(kon, "SELECT count(1) as jml FROM pukat_user_peran WHERE userid='" + userk + "' AND peran='OPERATOR'");
    if (hak <= 0) {
        response.sendRedirect("no.jsp");
    } else if (userid != null && jenis != null) {
        try {

            String b = request.getParameter("tanggal_transfer");
            String tahun = b.substring(6, 10);
            String bln = b.substring(3, 5);
            String tg = b.substring(0, 2);
            String tanggal = tahun + "-" + bln + "-" + tg;
            String id = kb.getStringSQL(kon, "SELECT id FROM pukat_pihak WHERE guid = '" + userid.toString() + "'");
            Object user = session.getAttribute("punyaku");
            String nilai = request.getParameter("nilai").toString();
            String cara = request.getParameter("tbb").toString();
            String[][] data = {
                {"id_pihak", id},
                {"id_tabungan", jenis.toString()},
                {"nilai", nilai},
                {"tanggal", tanggal},
                {"cara", cara},
                {"guid", userid.toString()},
                {"user", user.toString()},};
            boolean ok = kb.simpanData(kon, "pukat_tabungan_pihak_transfer", data, true);
            if (ok) {
                //    String iuran = request.getParameter("nilai").toString();
                //    String min = kb.getStringSQL(kon, "SELECT iuran FROM pukat_tabungan_pihak WHERE id_pihak = '" + id + "' LIMIT 1");
                //    int akhir = kb.getIntSQL(kon, "SELECT masuk FROM pukat_tabungan_pihak WHERE id_pihak = '" + id + "' AND tgl_masuk IS NOT NULL ORDER BY id DESC LIMIT 1");
                String belum = kb.getStringSQL(kon, "SELECT id FROM pukat_tabungan_pihak WHERE id_pihak = '" + id + "' AND tgl_masuk IS NULL ORDER BY id LIMIT 1");

                String[][] data2 = {
                    {"masuk", nilai},
                    {"cara_masuk", cara},
                    //    {"nilai", request.getParameter("nilai").toString()},
                    {"tgl_masuk", tanggal},
                    //     {"cara", request.getParameter("tbb").toString()},
                    //     {"guid", userid.toString()},
                    {"approval", user.toString()},};
                kb.updateData(kon, "pukat_tabungan_pihak", data2, "id = " + belum);
                //  int sisa = Integer.parseInt(min) - akhir;
                kb.setSQL(kon, "INSERT INTO pukat_log(log, nilai, user, from_date, id_pihak) VALUES('TRANSFER UANG','TRANSFER UANG SENILAI " + nilai + " DISETUJUI OLEH " + user + "', '" + user + "',now(),'" + id + "')");

                response.sendRedirect("suksesSimpan.jsp");
            } else {
                response.sendRedirect("gagalSimpan.jsp");
            }
        } catch (Exception e) {
            out.println("erororor");
            out.println(e.getMessage());
            System.out.println(e.getMessage());
            // response.sendRedirect("redirect.jsp");
        }
        kon.close();
    } else {
        String id_pihak = request.getParameter("a");
        String id_tabungan = request.getParameter("b");
        String nam = request.getParameter("c");
        String nom = request.getParameter("d");
        String saldo = request.getParameter("e");
        String min = kb.getStringSQL(kon, "SELECT DISTINCT iuran FROM pukat_tabungan_pihak a INNER JOIN pukat_pihak b ON b.id = a.id_pihak WHERE b.guid = '" + id_pihak + "' AND id_tabungan = '" + id_tabungan + "' LIMIT 1");
        //   out.print(id_pihak+" - "+min);
        kon.close();
        if (id_pihak == null) {
            response.sendRedirect("error.jsp");
        } else {


%>
<html>
    <head>
        <title>Input Data</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="asset/img/icon.png">
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <link href="css/login.css" rel="stylesheet" type="text/css"/>


        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/animate.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
        <link href="fonts/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/skp_home.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
        <script src="js/jquery.min.js" type="text/javascript"></script>
        <link href="css/form/sky-forms_1.css" rel="stylesheet" type="text/css"/>      
        <link href="css/form/sky-forms-green_1.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    </head>
    <body>
        <div class="container" style="margin-top: 0px;">
            <div class="panel panel-default">

                <div class="panel-body">

                    <form action="anggotaTransfer.jsp" autocomplete="on" method="post" accept-charset="UTF-8" role="form" class="sky-form">
                        <fieldset>
                            <input type="hidden" name="id_pihak" value="<%=id_pihak%>">
                            <input type="hidden" name="id_tabungan" value="<%=id_tabungan%>">
                            <label class="panel-login">
                                <div class="login_result">Nama Anggota</div>
                            </label>   
                            <label class="input">
                                <input name="nama" type="text" value="<%=nam%>" disabled>
                            </label>
                            <label class="panel-login">
                                <div class="login_result">Nomor Anggota</div>
                            </label>
                            <label class="input">
                                <input id="prog" name="prog" type="text" value="<%=nom%>" disabled>
                            </label>
                            <label class="panel-login">
                                <div class="login_result">Saldo Akhir</div>
                            </label>
                            <label class="input">
                                <input id="saldo" name="saldo" type="text" value="<%=saldo%>" disabled>
                            </label>
                        </fieldset>
                        <fieldset>
                            <div class="col-sm-7 col-xs-12">
                                <label class="panel-login">
                                    <div class="login_result">Melalui</div>
                                </label>
                                <label class="input">                       
                                    <select id="tbb" name="tbb" class="form-control" style="margin-bottom: 15px;" required>
                                        <option value="transfer" selected>&nbsp;&nbsp;Transfer ke Rekening BCA YPS</option>
                                        <option value="debet">&nbsp;&nbsp;Auto Debet dari Rekening BCA Anda&nbsp;&nbsp;</option>
                                    </select>                                
                                </label>
                            </div>
                            <div class="col-sm-1 hidden-xs">
                                &nbsp;&nbsp;
                            </div>
                            <div class="col-sm-4 col-xs-12">
                                <label class="panel-login">
                                    <div class="login_result">Tanggal Transfer</div>
                                </label>
                                <label class="input">
                                    <input type="text" name="tanggal_transfer" class="form-control" placeholder="DD/MM/YYYY" id="tanggal_mulai" required />
                                </label>
                            </div>
                            <div class="panel-12" style="margin-top: 10px;">
                                <label class="panel-login">Nilai Transfer (Rp.)</label>
                                <label class="input">
                                    <input name="nilai" type="number" value="<%=min%>" required />
                                </label>
                            </div>
                        </fieldset>
                        <footer>
                            <button type="submit" id="smk" class="button">Simpan Data</button>
                        </footer>
                    </form>


                </div>
            </div>
        </div>
        <script src='//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
        <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <script>
            $(function () {


                $("#tanggal_mulai").datepicker({
                    //   yearRange: '1910:2010',
                    //    changeMonth: true,
                    //    changeYear: true,
                    dateFormat: 'dd/mm/yy',
                    //    defaultDate: '01/01/1970'
                });

            });
        </script>

    </body>
</html>
<%    }
    }
%>