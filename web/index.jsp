<%-- 
    Document   : index
    Created on : Nov 21, 2016, 9:52:50 AM
    Author     : iweks
--%>
<%@page import="org.smi.koneksiDatabase"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.*"%>
<%@page import="org.smi.LoginSystem"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="img/logo.png">
        <title>PUKAT SEMARANG</title>
        <link href="css/material-dashboard.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">

        <!-- Custom styling plus plugins -->
        <link href="css/styles.css" rel="stylesheet" type="text/css"/>
        <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
        <link href="css/animate.min.css" rel="stylesheet">
        <link href="css/custom.css" rel="stylesheet" type="text/css"/>
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="css/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
        <link href="css/skp_home.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.3.css" />
        <link href="css/icheck/flat/green.css" rel="stylesheet" />
        <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />


        <script src="js/jquery.min.js"></script>
        <script src="js/nprogress.js"></script>
        <link href="css/login.css" rel="stylesheet" type="text/css"/>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>


    </head>
    <body style="font-family: 'Calibri'; background-color: #EEEEEE;">
        <%
            String abgt = request.getRequestURL().toString();
            String user = request.getParameter("username");
            String pass = request.getParameter("sandi");
            String html = "login.jsp";
            try {

                Object punyaku = session.getAttribute("punyaku");

                if (punyaku != null) {
                    html = "sukses.jsp";
                } else {
                    session.setAttribute("punyaku", user);
                    if (user != null && pass != null) {

                        koneksiDatabase kb = new koneksiDatabase();
                        Connection kon = kb.getKoneksiSystem();
                        int ok = kb.getIntSQL(kon, "SELECT id_pihak FROM pukat_user WHERE userid = '" + user
                                + "' AND pass = MD5('" + pass + "')");
                        if (ok == 0) {
                            // session.setAttribute("pesanError", "Gagal Login!!! User dan Password Anda tidak sesuai");
                            // session.removeAttribute("punyaku");
                            response.sendRedirect("http://pukatsemarang.org");
                        } else {
                            String userAgent = request.getHeader("User-Agent");
                            String userKu = userAgent.toLowerCase();
                            String os = "Other";
                            String browser = userKu;

                            if (userAgent.toLowerCase().indexOf("windows nt 10") >= 0) {
                                os = "Windows 10";
                            } else if (userAgent.toLowerCase().indexOf("iphone") >= 0) {
                                os = "iPhone";
                            } else if (userAgent.toLowerCase().indexOf("windows nt 6.3") >= 0) {
                                os = "Windows 8.1";
                            } else if (userAgent.toLowerCase().indexOf("windows nt 6.2") >= 0) {
                                os = "Windows 8";
                            } else if (userAgent.toLowerCase().indexOf("windows nt 6.0") >= 0) {
                                os = "Windows 7";
                            } else if (userAgent.toLowerCase().indexOf("linux") >= 0) {
                                os = "Linux";
                            } else if (userAgent.toLowerCase().indexOf("android") >= 0) {
                                os = "Android";
                            } else if (userAgent.toLowerCase().indexOf("blackberry") >= 0) {
                                os = "Blackberry";
                            } else if (userAgent.toLowerCase().indexOf("mac") >= 0) {
                                os = "Mac OS";
                            } else if (userAgent.toLowerCase().indexOf("X11") >= 0) {
                                os = "Unix";
                            }

                            String ip = request.getRemoteAddr();
                            String[][] data = {
                                {"id_pihak", String.valueOf(ok)},
                                {"userid", user},
                                {"ip", ip},
                                {"os", os},
                                {"browser", browser},};
                            kb.simpanData(kon, "pukat_user_aktifitas", data, true);
                            html = "sukses.jsp";
                            session.setAttribute("punyaku", user);
                            session.removeAttribute("pesanError");
                        }
                        kon.close();
                    } else {
                        response.sendRedirect("http://pukatsemarang.org");
                    }

                }

            } catch (Exception e) {
                out.print(e.getMessage());
                out.print("SISTEM SEDANG SIBUK... MOHON DIULANGI BEBERAPA SAAT LAGI");
            }

        %>

        <jsp:include page="<%=html%>" />
        <div class="col-lg-12" style="text-align: center">
            <footer style="background-color: #EEEEEE">Copyright &#169; Yayasan Pukat Semarang - Power By <a href="//no.com" target="_blank">XXX</a></footer>
        </div>
        <!--Start of Tawk.to Script-->
        <script type="text/javascript">
            var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
            (function () {
                var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
                s1.async = true;
                s1.src = 'https://embed.tawk.to/596882841dc79b329518e3e6/default';
                s1.charset = 'UTF-8';
                s1.setAttribute('crossorigin', '*');
                s0.parentNode.insertBefore(s1, s0);
            })();
        </script>
        <!--End of Tawk.to Script-->
    </body>
</html>