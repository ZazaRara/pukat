<%-- 
    Document   : redirect
    Created on : Nov 29, 2016, 8:26:24 AM
    Author     : iweks
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body style="min-height: 100px; background-color: white;">
        <h1>Session Anda telah habis... mohon tunggu...</h1>
    </body>
</html>
<script src="js/jquery.min.js"></script>
<script type="text/javascript">
    $(function () {
        window.top.location.href = "index.jsp";
    });
</script>
